function main(req,resp){
	var url=req.getServletPath();
	if(url.startsWith("/jse/")||url.startsWith("/api/")||url=="/login"){
		return true;
	}
	if(req.getSession().getAttribute("admin")!=null){
		return true;
	}
	let uid = req.getParameter("uid")
	if(!isEmpty(uid)){
		req.getSession().setAttribute("admin",dao.fetch("sys_user",Cnd.where("id","=",uid)))
		return true;
	}
	let urls=url.split("/");
	let id=urls[urls.length-1];
	var n = parseInt(id);
	var set=new Set();
	set.add("/app/info/"+id);
	set.add("/app/device/"+id);
	set.add("/app/discenr/"+id);
	set.add("/app/screen/"+id);
	set.add("/app/user/"+id);
	set.add("/app/runinfo/"+id);
	set.add("/app/small/"+id)
	if(!isNaN(n)&&set.has(url)){
		req.getSession().setAttribute("admin",dao.fetch("sys_user",Cnd.where("id","=",id)))
		return true;
	}
	return ">>:/login";
}