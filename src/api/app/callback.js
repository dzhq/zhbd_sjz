//@json
function main(tbl){
	tbl.remove("faces")
	print("callback param:"+tbl)
	//print("call------------------"+tbl.result)
	if(isEmpty(tbl.result)||isEmpty(tbl.result.person_id)){
		print("---------------------------not tbl.result.person_id user_id")
		return {code:-1,msg:"not tbl.result.person_id"}
	}
	let app_device = dao.fetch("app_device",Cnd.where("device_id","=",tbl.cid));
	if(app_device==null){
		return {code:-1,msg:"摄像头未配置"}
	}
	let app_user=dao.fetch("app_user",Cnd.where("id","=",tbl.result.user_id));
	if(app_user==null){
		let person=dao.fetch("select user_id from app_person",Cnd.where("person_id","=",tbl.result.person_id));
		if(person==null){
			person={user_id:0}
		}
		app_user = dao.fetch("app_user",Cnd.where("id","=",person.user_id));
		if(app_user==null){
			app_user=dao.fetch("app_user",Cnd.where("phone","=",tbl.result.person_name)
			.and("dept_id","=",app_device.dept_id))
		}
		if(app_user==null){
			print("---------------------------person_id:"+person.person_id+" and person_name is not user!");
			return {code:-1,msg:"not user"}
		}
	}
	
	
	let time = tbl.time*1000;
	let app_discern = {
		table:"app_discern",
		device_id: app_device.device_id,
		info_id: app_user.info_id,
		position: app_device.position,
		user_id: app_user.id,
		code: app_user.code,
		username: app_user.username,
		avator: app_user.avator
	}
	//查询该用户的上一条识别记录是哪条
	let previous_discern = dao.fetch("app_discern",Cnd.where("user_id","=",app_user.id).desc("id"));
	//因为用户是跟步道绑定的，所以只要是同一个用户那必然是同一条步道，用户的唯一约束是步道id+用户手机号
	if(previous_discern == null){
		//如果是空的,则为新运动
		app_discern.type = 0;
		print("trail_id:"+app_user.info_id+" >>>> user_id:"+app_user.id + " >>>> username:"+app_user.username + ">>>> start runing")
	}else if(tbl.time - previous_discern.add_time.time/1000 <= 20*60 && tbl.cid == previous_discern.device_id){
		app_discern.id = previous_discern.id;
		app_discern.type = previous_discern.type;
		app_discern.update_time=new java.util.Date();
	}else if(tbl.time - previous_discern.add_time.time/1000 <= 20*60){
		//继续运动
		app_discern.type = 1;
		//获取两个摄像头之间的距离
		let app_device_conf = dao.fetch("app_device_conf", Cnd.where("device_id1","=",tbl.cid).and("device_id2","=",previous_discern.device_id));
		if(app_device_conf == null)
			app_device_conf = dao.fetch("app_device_conf", Cnd.where("device_id2","=",tbl.cid).and("device_id1","=",previous_discern.device_id));
		if(app_device_conf == null)
			print("error>>>> device not config")
		else{

			let length = app_device_conf.distance;//长度
			let time = tbl.time - previous_discern.add_time.time/1000;//运动时长
			let kcal = length / 1000 * app_user.weight *1.036;//消耗的卡路里
			let speed = length/time;//运动速度
			let step = 0;
			if(speed <= 0.5){
				//走路
				app_discern.run_type = 0;
				step = length/0.7;
			}else if(speed <= 3){
				//跑步
				app_discern.run_type = 1;
				step = length/0.5;
			}else{
				//冲刺
				app_discern.run_type = 2;
				step = length/1.2;
			}
				
			app_discern.length = length;
			app_discern.speed = speed;
			app_discern.kcal = kcal;
			app_discern.run_date = new Date().toLocaleDateString();//获取当天日期
			app_discern.run_time = time;
			

			// 更新到运动记录的数据库里 每天一条数据，不断更新
			//判断当天有没有运动数据
			let app_runinfo = dao.fetch("app_runinfo", Cnd.where("user_id","=",app_user.id).and("run_date","=",app_discern.run_date)) || {};
			app_runinfo.table = "app_runinfo";
			app_runinfo.info_id =  app_user.info_id;
			app_runinfo.dept_id =  app_user.dept_id;
			app_runinfo.user_id =  app_user.id;
			app_runinfo.username =  app_user.username;
			app_runinfo.phone =  app_user.phone;
			app_runinfo.mileage =  length + (app_runinfo.mileage||0);
			app_runinfo.run_date = app_discern.run_date;
			app_runinfo.kcal =  kcal + (app_runinfo.kcal||0);
			app_runinfo.run_time = time + (app_runinfo.run_time||0);
			app_runinfo.speed =  app_runinfo.mileage / app_runinfo.run_time;
			app_runinfo.code =  app_user.code;
			app_runinfo.run_type = 2;//冲刺
			if(app_runinfo.speed <= 0.5)//走路
				app_runinfo.run_type = 0;
			else if(app_runinfo.speed <= 3)//跑步
				app_runinfo.run_type = 1;
			app_runinfo.step = step + (app_runinfo.step||0) ;//步数，根据运动类型身高和距离判断
			app_runinfo.avator = app_user.avator;
			let runinto=dao.mager(app_runinfo);
			//判断用户有没有制定过运动计划，如果有的话 更新计划日志表
			let plan = dao.fetch("app_plan",Cnd.where("user_id","=",app_user.id));
			if(plan != null){
				//判断当天有没有数据
				let plan_log = dao.fetch("app_plan_log", Cnd.where("user_id","=",app_user.id).and("run_date","=",app_discern.run_date)) || {};
				plan_log.table = "app_plan_log";
				plan_log.user_id = app_user.id;
				plan_log.run_date = app_discern.run_date;
				plan_log.plan_type = plan.type;
				plan_log.plan_number = plan.number;
				if(plan.type == 0 )
					plan_log.number = app_runinfo.mileage;
				else if(plan.type == 1 )
					plan_log.number = app_runinfo.run_time;
				else
					plan_log.number = app_runinfo.kcal;
				plan_log.is_complete = 0;
				if(plan_log.number >= plan.number)
					plan_log.is_complete = 1;
				dao.mager(plan_log);
			}
			dao.mager(app_discern);
			return {code:0,data:runinto};
		}
		//print("trail_id:"+app_user.info_id+" >>>> user_id:"+app_user.id + " >>>> username:"+app_user.username + ">>>> start go on")
	}else{
		//新运动
		app_discern.type = 0;
		//print("trail_id:"+app_user.info_id+" >>>> user_id:"+app_user.id + " >>>> username:"+app_user.username + ">>>> start runing")
	}
	dao.mager(app_discern);
	return {code:2,msg:"无运动记录"}
}