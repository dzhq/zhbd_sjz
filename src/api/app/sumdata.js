//@json
function main(tbl){
	let dept_id=tbl.dept_id;
	if(isEmpty(tbl.dept_id)){
		let dept=dao.fetch("select dept_id from app_screen",Cnd.where("imei","=",tbl.imei));
		if(dept==null){
			print("=============imei not:"+tbl.imei)
			return {
		male:0,	//男用户
		female:0,	//女用户
		sum_runinfo:0,	//步道使用人次
		mileages:0,	//累计里程数
		steps:0,	//累计步数
		kcals:0,	//累计消耗热量
		speeds:0,	//平均速度
		run_times:0,	//累计运动时间
		age18:0,	//18岁以下用户
		age35:0,	//18-35岁用户
		age50:0,	//35-50岁用户
		agemax:0,	//70岁以上用户
		type0:0,	//一周内走路人数
		type1:0	//一周内跑步人数
	};
		}
		dept_id = dept.dept_id;
		tbl.put("dept_id",dept_id);
	}
	if(cache.get("sumdata"+dept_id)!=null){
		return cache.get("sumdata"+dept_id);
	}
	//let date_d = Times.format(Times.date(Times.utc()*1000+"")).substring(0,10);
	print()
	let date_w = Times.format(Times.date(java.lang.Long.valueOf((System.currentTimeMillis()/1000-604800)*1000+""))).substring(0,10);
	
	//let date_m = Times.format(Times.date((Times.utc()-2592000)*1000+"")).substring(0,10);
	//APP的数据接口  通过imei获取数据（理由：给客户提供接口的时候 不会泄露我们的dept_id 值）
	
	//获取男性用户数量
	let male = dao.queryForMap("SELECT COUNT(id) as male FROM app_user WHERE sex = '男'  AND dept_id = " + dept_id);
	//获取女性用户数量
	let female = dao.queryForMap("SELECT COUNT(id) as female FROM app_user WHERE sex = '女'  AND dept_id = " + dept_id);
	//累计运动人/次
	let sum_runinfo = dao.queryForMap("SELECT COUNT(id) as sum_runinfo,SUM(mileage) AS mileages,SUM(step) AS steps,SUM(kcal) AS kcals,AVG(speed) AS speeds,SUM(run_time) AS run_times FROM app_runinfo WHERE dept_id = " + dept_id);
	
	//获取不同年龄段的用户数量
	let age18 = dao.queryForMap("SELECT COUNT(id) as nums FROM app_user WHERE age <= 18  AND dept_id = " + dept_id);
	let age35 = dao.queryForMap("SELECT COUNT(id) as nums FROM app_user WHERE age > 18 AND age <= 35 AND dept_id = " + dept_id);
	let age50 = dao.queryForMap("SELECT COUNT(id) as nums FROM app_user WHERE age > 35 AND age <= 50  AND dept_id = " + dept_id);
	let agemax = dao.queryForMap("SELECT COUNT(id) as nums FROM app_user WHERE age > 50 AND dept_id = " + dept_id);
	
	//运动方式统计，按周

	let type0 = dao.query("SELECT COUNT(id) as type0 , run_date FROM app_runinfo WHERE run_type = 0  AND dept_id = " + dept_id + " AND run_date > '" + date_w + "' GROUP BY run_date");
	let type1 = dao.query("SELECT COUNT(id) as type1 , run_date FROM app_runinfo WHERE run_type >0  AND dept_id = " + dept_id + " AND run_date > '" + date_w + "' GROUP BY run_date");
	
	
	let data = {
		male:male.male,	//男用户
		female:female.female,	//女用户
		sum_runinfo:sum_runinfo.sum_runinfo,	//步道使用人次
		mileages:sum_runinfo.mileages,	//累计里程数
		steps:sum_runinfo.steps,	//累计步数
		kcals:sum_runinfo.kcals,	//累计消耗热量
		speeds:sum_runinfo.speeds,	//平均速度
		run_times:sum_runinfo.run_times,	//累计运动时间
		age18:age18.nums,	//18岁以下用户
		age35:age35.nums,	//18-35岁用户
		age50:age50.nums,	//35-50岁用户
		agemax:agemax.nums,	//70岁以上用户
		type0:type0,	//一周内走路人数
		type1:type1	//一周内跑步人数
	};
	cache.put("sumdata"+tbl.dept_id,data,600000)
	return data;
}