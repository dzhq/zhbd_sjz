//@json
function main(tbl){
	print("用户更新后列表参数:"+tbl)
	//1 根据时间获取更新时候后的所有用户信息
	let page=tbl.page||1;
	let limit=tbl.limit||10;
	let dept_id=tbl.dept_id;
	if(isEmpty(dept_id)){
		dept_id=dao.fetch("app_info",Cnd.where("id","=",tbl.trail_id)).dept_id;
	}
	if(isEmpty(dept_id)){
		return {code:-1,msg:"同步失败单位id为空!"}
	}
	return {code:0,data:dao.query("app_user",Cnd.where("dept_id","=",dept_id).and("update_time",">",tbl.update_time||"2021-05-01 00:00:00"),page,limit)||[]}
}