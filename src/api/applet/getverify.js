//@json
function main(tbl){
	let timestamp = Times.time();
	let param = Math.ceil(Math.random()*10000);
	if(param<1000||param>9999)
		param = Math.ceil(Math.random()*10000);
	let params = {
		"accountSid":"970ae583ee329f6b6dca5d29502888c6",
		"templateid":"272206",
		"to":tbl.phone,
		"timestamp":timestamp,
		"sig":Lang.md5("970ae583ee329f6b6dca5d29502888c6455722c1efd0312ff4daf18602470730"+timestamp),
		"param":param+""
	}

	let data = JSON.parse(Http.post("https://openapi.danmi.com/distributor/sendSMS",params));
	
	if(data.respCode == "0000"){
		cache.put(tbl.phone,param);
		return {code:0,msg:"验证码发送成功"};
	}
	return {code:-1,msg:"验证码发送失败，稍后重试",data:data};
}