//@json
function main(tbl){
	//微信小程序登录接口
	let depts = dao.query("sys_dept",Cnd.where("app_id","=",tbl.app_id));
	if(depts==null)return {code:-1,msg:"无消息"}
	let sys_dept=depts[0];
	//根据appid 查询 appid和secret （未来小程序的菜单都是通过后台配置的动态菜单）
	let url = "https://api.weixin.qq.com/sns/jscode2session?appid="+tbl.app_id+"&secret="
	+sys_dept.app_secret+"&js_code="+tbl.js_code+"&grant_type=authorization_code";
	let data = JSON.parse(Http.get(url));
	let config = {
			"img":sys_dept.img,
			"applet_title":sys_dept.applet_title,
			"copyright":sys_dept.copyright,
			"person":sys_dept.person,
			"phone":sys_dept.phone,
			"dept_id":sys_dept.id
	}
	let ids=new ArrayList();
	depts.forEach(function(x){
		ids.add(x.id)
	})
	let app_info = dao.query("app_info",Cnd.where("dept_id","in",ids));
	return {code:0,data:data,config:config,trails:app_info}
}