//@json
function main(tbl){
	let version=cache.get("applet.version")
	if(version==null){
		version=dao.select("select value from sys_config where `key`='applet.version'","string")
		cache.put("applet.version",version)
	}
	if(tbl.option=="add"){
		version=dao.select("select value from sys_config where `key`='applet.version'","string")
		version=parseInt(version)+1;
		cache.put("applet.version",version)
		dao.update("sys_config",{value:version},Cnd.where("`key`","=","applet.version"))
	}else if(tbl.option=="sub"){
		version=dao.select("select value from sys_config where `key`='applet.version'","string")
		version=parseInt(version)-1;
		cache.put("applet.version",version)
		dao.update("sys_config",{value:version},Cnd.where("`key`","=","applet.version"))
	}
	return {code:0,data:version}
}