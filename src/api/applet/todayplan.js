//@json
function main(tbl){
	let app_plan_log = dao.fetch("app_plan_log",Cnd.where("user_id","=",tbl.user_id).and("run_date","=",tbl.run_date));
	let app_runinfo = dao.fetch("app_runinfo",Cnd.where("user_id","=",tbl.user_id).and("run_date","=",tbl.run_date));
	
	if(app_plan_log == null)
		return {code:0,data:{}}
	return {code:0,data:{
		is_complete:app_plan_log.is_complete,//计划完成情况，0未完成，1完成
		type:app_plan_log.plan_type,//计划类型
		plan_number:app_plan_log.plan_number,//计划值
		number:app_plan_log.number,//完成值
		run_time:app_runinfo.run_time,//当天运动时长
		speed:app_runinfo.speed,//平均速度
		mileage:app_runinfo.mileage,//里程
		kcal:app_runinfo.kcal,//消耗热量
		step:app_runinfo.step//跑步步数
	}}
}