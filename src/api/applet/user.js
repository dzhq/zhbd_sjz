//@json
function main(tbl){
	let users = dao.query("app_user",Cnd.where("openid","=",tbl.openid));
	if(isEmpty(users)){
		let depts=dao.query("sys_dept")
		depts.forEach(function(d){
			if(d.applets){
				d.applets.forEach(function(a){
					if(a.type=="智慧步道"){
						a.put("infos",dao.query("app_info",Cnd.where("dept_id","=",d.id)))
					}
				})
			}
		})
		return {code:0,depts:depts,users:[]}
	}
	users.forEach(function(x){
		let d=dao.fetch("sys_dept",Cnd.where("id","=",x.dept_id));
		if(d.applets){
				d.applets.forEach(function(a){
					if(a.type=="智慧步道"){
						a.put("infos",dao.query("app_info",Cnd.where("dept_id","=",d.id)))
					}
				})
		}
		x.put("dept",d)
	})
	return {code:0,users:users,depts:dao.query("sys_dept")};
}