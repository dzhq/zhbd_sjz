//@json
function main(tbl){
	//获取用户一周的运动数据
	let time = (System.currentTimeMillis()/1000-2592000)*1000;
	let date = Times.format(Times.date(time)).substring(0,10);
	let data = dao.query("app_runinfo",Cnd.where("user_id","=",tbl.user_id).and("run_date",">",date).desc("run_date"),1,30);
	//,"run_time,run_type,speed,kcal,step,mileage"
	let idx = 0;
	time = Times.utc()*1000
	let runinfos = [];
	
	
	for(let i = 0 ; i<30 ; i++){
		date = Times.format(Times.date(time)).substring(0,10);
		if(data.length > 0 && data[idx].run_date == date){
			runinfos.push(data[idx]);
			idx = idx < data.length - 1 ? idx + 1 : 0;
		}else{
			//插入一条空数据
			let nulldata = {
					run_time:0,
					run_type:2,
					speed:0,
					kcal:0,
					step:0,
					mileage:0,
					run_date:date
			};
			runinfos.push(nulldata);
		}
		time = time - 24*60*60*1000;
	}
	
	return {code:0,data:runinfos}
}