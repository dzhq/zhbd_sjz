//@json
function main(tbl){
	let dept_id=tbl.dept_id;
	if(isEmpty(tbl.dept_id)){
		dept_id=dao.fetch("app_info",Cnd.where("id","=",tbl.trail_id),"dept_id").dept_id;
		tbl.put("dept_id",dept_id);
	}
	//里程榜  一天的，一周的，一个月的
	let date_d = Times.format(Times.date(Times.utc()*1000+"")).substring(0,10);
	let date_w = Times.format(Times.date((Times.utc()-604800)*1000+"")).substring(0,10);
	let date_m = Times.format(Times.date((Times.utc()-2592000)*1000+"")).substring(0,10);
	
	let sql_d = "SELECT user_id,username,avator, MAX(speed) AS maxspeed FROM app_runinfo WHERE run_date = '"+ date_d +"' AND dept_id = "+dept_id+" GROUP BY user_id,username,avator ORDER BY maxspeed DESC LIMIT 0,10";
	let sql_w = "SELECT user_id,username,avator, MAX(speed) AS maxspeed FROM app_runinfo WHERE run_date > '"+ date_w +"' AND dept_id = "+dept_id+" GROUP BY user_id,username,avator ORDER BY maxspeed DESC LIMIT 0,10";
	let sql_m = "SELECT user_id,username,avator, MAX(speed) AS maxspeed FROM app_runinfo WHERE run_date > '"+ date_m +"' AND dept_id = "+dept_id+" GROUP BY user_id,username,avator ORDER BY maxspeed DESC LIMIT 0,10";

	
	return {code:0,date_d:dao.query(sql_d),date_w:dao.query(sql_w),date_m:dao.query(sql_m)}
}
