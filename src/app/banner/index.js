function main(tbl,req,resp){
	let page=tbl.page||0;
	let limit=tbl.limit||10
	let cnd=new Cnd();
	if(!isEmpty(tbl.title)){
		cnd.and("Title","like","%"+tbl.title+"%")
	}
	return dao.pager("app_banner",cnd.desc("id"),page,limit)
}