//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(sattr("admin").role_id!=1){
		cnd.and("code","=",sattr("admin").code)
	}
	if(!isEmpty(tbl.name)){
		cnd.and("name","like","%"+tbl.name+"%")
	}
	if(!isEmpty(tbl.city)){
		cnd.and("city","like",tbl.city+"%")
	}
	if(!isEmpty(tbl.add_time)){
		cnd.and("add_time",">",tbl.add_time[0]+" 00:00:00")
		.and("add_time","<",tbl.add_time[1]+" 23:59:59")
	}
	let page=tbl.page||1;
	let limit=tbl.limit||10;
	var pager=dao.pager("app_discern",cnd.desc("id"),page,limit);
	return pager;
}