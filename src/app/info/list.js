//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(sattr("admin").role_id!=1){
		cnd.and("code","=",sattr("admin").code)
	}
	if(!isEmpty(tbl.name)){
		cnd.and("name","like","%"+tbl.name+"%")
	}
	if(!isEmpty(tbl.company)){
		cnd.and("company","like","%"+tbl.company+"%")
	}

	let page=tbl.page||1;
	let limit=tbl.limit||10;
	var pager=dao.pager("app_info",cnd.desc("id"),page,limit);
	return pager;
}