//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(sattr("admin").role_id!=1){
		cnd.and("code","=",sattr("admin").code)
	}
	if(!isEmpty(tbl.info_id)){
		cnd.and("info_id","=",tbl.info_id)
	}
	
	let page=tbl.page||1;
	let limit=tbl.limit||10;
	var pager=dao.pager("app_screen",cnd.desc("id"),page,limit);
	pager.data.forEach(function(s){
		s.put("switch_open",0)
		if(isEmpty(s.switch_id)){
			s.put("switch_status",-1)
		}else{
			try{
				let status=com.hr.Switch.status(s.switch_id);
				s.put("switch_status",status)
				if(status==1){
					let switch_open=com.hr.Switch.openStatus(s.switch_id);
					s.put("switch_open",switch_open)
				}
			}catch(e){}
		}
	})
	return pager;
}