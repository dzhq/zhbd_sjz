function main(tbl){
	var where_str = "";
	if(sattr("admin").role_id!=1){
		where_str = " where code like '%" + sattr("admin").code + "%'"
	}
	
	let depts=new ArrayList();
	depts.add(java.util.Map.of("value","","label","全部"))
	depts.addAll(dao.query("select `id` as 'value',`name` as 'label' from sys_dept" + where_str))
	attr("depts",depts)
	
	let codes=new ArrayList();
	codes.add(java.util.Map.of("value","","label","全部"))
	codes.addAll(dao.query("select `code` as 'value',`name` as 'label' from sys_dept" + where_str))
	attr("codes",codes)
	let unit= dao.fetch("app_small",Cnd.where("id","=",tbl.id))
	return unit||{};
}