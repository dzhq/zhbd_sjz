//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1)
	if(sattr("admin").role_id!=1){
		cnd.and("code","=",sattr("admin").code)
	}
	if(!isEmpty(tbl.title)){
		cnd.and("title","like","%"+tbl.title+"%")
	}
	if(!isEmpty(tbl.imei)){
		cnd.and("imei","=",tbl.imei)
	}

	let page=tbl.page||1;
	let limit=tbl.limit||10;
	var pager=dao.pager("app_small",cnd.desc("id"),page,limit);
	return pager;
}