package com.hr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.arcsoft.face.ActiveDeviceInfo;
import com.arcsoft.face.ActiveFileInfo;
import com.arcsoft.face.AgeInfo;
import com.arcsoft.face.EngineConfiguration;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.FaceSimilar;
import com.arcsoft.face.FunctionConfiguration;
import com.arcsoft.face.GenderInfo;
import com.arcsoft.face.Rect;
import com.arcsoft.face.VersionInfo;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ExtractType;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.jse.Jse;

public class Face {

//	private static final String APPID =  "5gTJ7MDf9zgPHo9cK2bNJLFTg4LCnqMNi3KMc6j8Ktb8";
//	private static final String SDKKEY = "7NT3nyRWxm6G6DNcuY6mDQcBXFQZF9aDkrXG2r2VmNEh";
//	private static final String ACTIVEKEY = "86L1-118D-G11G-X5H2";// benji:"86L1-118D-G12W-61BE";//

	private FaceEngine faceEngine;

	public Face() {
		this.faceEngine = new FaceEngine(Jse.conf.getString("face.path"));
//		this.faceEngine = new FaceEngine("D:\\work\\zhbd_sjz\\src\\win64");
		
		// 激活引擎
		int errorCode = 0;

//        ActiveDeviceInfo activeDeviceInfo = new ActiveDeviceInfo();
        //采集设备信息（可离线）
//        errorCode = faceEngine.getActiveDeviceInfo(activeDeviceInfo);
//        System.out.println("engine info errorCode:" + errorCode);
//        System.out.println("engine info:" + activeDeviceInfo.getDeviceInfo());

        ActiveFileInfo activeFileInfo = new ActiveFileInfo();
        errorCode = faceEngine.getActiveFileInfo(activeFileInfo);
        if(errorCode!=0) {
        	errorCode = faceEngine.activeOnline(Jse.conf.getString("face.appid"),
    				Jse.conf.getString("face.sdkkey"),
    				Jse.conf.getString("face.activekey"));
        }
        System.out.println("engine file errorCode:" + errorCode);
        System.out.println("engine file:" + activeFileInfo.toString());
//        System.out.println("获取激活文件errorCode:" + errorCode);
//        System.out.println("激活文件信息:" + activeFileInfo.toString());
//        int errorCode = faceEngine.activeOnline(Jse.conf.getString("face.appid"),
//				Jse.conf.getString("face.sdkkey"),
//				Jse.conf.getString("face.activekey"));
//        System.out.println("face engine errorCode:" + errorCode);

        //引擎配置
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
        engineConfiguration.setDetectFaceMaxNum(10);
        //功能配置
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportAge(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportGender(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        functionConfiguration.setSupportImageQuality(true);
        functionConfiguration.setSupportMaskDetect(true);
        functionConfiguration.setSupportUpdateFaceData(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);

        //初始化引擎
        errorCode = faceEngine.init(engineConfiguration);
//        System.out.println("初始化引擎errorCode:" + errorCode);
        VersionInfo version = faceEngine.getVersion();
//        System.out.println(version);
	}
	
	private static Face face=new Face();
	
	public static Face get() {
		return face;
	}

	/**
	 * 判断人脸照片是否合格
	 * 
	 * @param filePath 人脸照片的路径
	 * @return 是否合格
	 * @throws IOException 
	 */
	public Map<String, Object> pass(String filePath) throws IOException {
		Map<String, Object> result = new HashMap<String, Object>();
		
		 //人脸检测
		File imageFile = new File(filePath);
        ImageInfo imageInfo = ImageFactory.getRGBData(imageFile);
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        int errorCode = faceEngine.detectFaces(imageInfo, faceInfoList);
        if(errorCode == 0) {
        	if(faceInfoList.size() != 1) {
            	result.put("code", "-1");
    			result.put("msg", "\\u8bf7\\u4e0a\\u4f20\\u6709\\u4e14\\u53ea\\u5305\\u542b\\u4e00\\u5f20\\u4eba\\u8138\\u7684\\u7167\\u7247");
    			return result;
            }
        	//特征提取
            FaceFeature faceFeature = new FaceFeature();
            errorCode = faceEngine.extractFaceFeature(imageInfo, faceInfoList.get(0), ExtractType.REGISTER, 0, faceFeature);
            if(errorCode == 0) {
            	//人脸属性检测
                FunctionConfiguration configuration = new FunctionConfiguration();
                configuration.setSupportAge(true);
                configuration.setSupportGender(true);
                configuration.setSupportLiveness(true);
                configuration.setSupportMaskDetect(true);
                errorCode = faceEngine.process(imageInfo, faceInfoList, configuration);
                if(errorCode == 0) {
                	//性别检测
                    List<GenderInfo> genderInfoList = new ArrayList<GenderInfo>();
                    errorCode = faceEngine.getGender(genderInfoList);
                    
                    //年龄检测
                    List<AgeInfo> ageInfoList = new ArrayList<AgeInfo>();
                    errorCode = faceEngine.getAge(ageInfoList);
                    
                    result.put("code", "0");
        			result.put("msg", "\\u7167\\u7247\\u4e0a\\u4f20\\u6210\\u529f");
        			result.put("gender", genderInfoList.get(0).getGender() == 1 ? "女" : "男");
        			result.put("age", ageInfoList.get(0).getAge());
        			
        			

        			//获取人脸位置
        			FaceInfo faceInfo = faceInfoList.get(0);
        			//获取人脸再照片中的位置
        			Rect rect = faceInfo.getRect(); 
        			//获取上传照片的尺寸
        			int width = imageInfo.getWidth();
        			int height = imageInfo.getHeight();

        			//判断人脸距离照片边缘的位置 距离太远则剪裁一下
        			int x,y,w,h = 0; //剪裁的x轴 y轴 宽度 高度
        			x = rect.left > 50 ? rect.left - 50 : 0;
        			y = rect.top > 50 ? rect.top - 50 : 0;
        			w = rect.right  + 50 < width ? rect.right - x + 50 : width - x;
        			h = rect.bottom  + 50 < height ? rect.bottom - y + 50 : height - y;
        			
        			if(cut(imageFile,x,y,w,h,imageFile)) {
        				result.put("code", "0");
        				result.put("facepath",filePath);
        			}else {
        				result.put("code", "-1");
        				result.put("msg", "\\u7167\\u7247\\u88c1\\u526a\\u5931\\u8d25");
        			}
                }else {
                	result.put("code", "-1");
        			result.put("msg", "\\u7167\\u7247\\u88c1\\u526a\\u5931\\u8d25");
                } 
            }else {
            	result.put("code", "-1");
    			result.put("msg", "\\u7167\\u7247\\u88c1\\u526a\\u5931\\u8d25");
            }
        }else {
        	result.put("code", "-1");
			result.put("msg", "\\u7167\\u7247\\u88c1\\u526a\\u5931\\u8d25");
        }
        
		return result;
	}
	
	/**
	 * 获取人脸特征值
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public String getFaceToken(String filePath) throws IOException {
		//人脸检测
		File imageFile = new File(filePath);
        ImageInfo imageInfo = ImageFactory.getRGBData(imageFile);
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        int errorCode = faceEngine.detectFaces(imageInfo, faceInfoList);
        if(errorCode == 0) {
        	//特征提取
            FaceFeature faceFeature = new FaceFeature();
            errorCode = faceEngine.extractFaceFeature(imageInfo, faceInfoList.get(0), ExtractType.REGISTER, 0, faceFeature);
            if(errorCode == 0) {
            	return Base64.getEncoder().encodeToString(faceFeature.getFeatureData());
            }
        }
		return null;
	}
	
	
	/**
	 * java 裁剪图片
	 * @param srcImageFile
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param destImageFile
	 * @return
	 */
	 public boolean cut(File srcImageFile, int x,int y,int width,int height,File destImageFile){
        try {
            //使用ImageIO的read方法读取图片
            BufferedImage read = ImageIO.read(srcImageFile);
            //调用裁剪方法
            BufferedImage image = read.getSubimage(x, y, width, height);
            //获取到文件的后缀名
            String fileName = srcImageFile.getName();
            String formatName = fileName.substring(fileName.lastIndexOf(".") + 1);
            //使用ImageIO的write方法进行输出
            ImageIO.write(image,formatName,destImageFile);
        } catch (IOException e) {
            e.printStackTrace();
            return  false;
        }
        return true;
	}
	 
	 public String convertFileToBase64(String imgPath) {
	        byte[] data = null;
	        // 读取图片字节数组
	        try {
	            InputStream in = new FileInputStream(imgPath);
	            data = new byte[in.available()];
	            in.read(data);
	            in.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        // 对字节数组进行Base64编码，得到Base64编码的字符串

	        return Base64.getEncoder().encodeToString(data);
	    }

	public static void main(String[] args) throws IOException {
//		Map map = face.pass("D:/1.jpg");
//		Record r= Jse.dao.fetch("app_user",Cnd.where("id","=",3470));
//		System.out.println(r.getString("face_token"));
		String token="AID6RAAAnEIV5Be98PM1PSAaiT3XXw69/zgzvUy4Er386zy9/B6MvIR1tTx2n0w9MYYxvXP6Br1JAwK+K6LoPKNzdT3D+e49ENcIvbuZWr01ZpS9HhM1u52Zhj08nju9pBXUPNUVVL2OcEU9aAqnPdO7Nb0FgoY8F+VaPPWSZ71djwe+y0ynPeQghj0eTIq9UZZnvR52Rbyx9qi9CO6VPReBND2u6sw9cyYQPAP0fr1AFwW9xbLPveiiHL2AcB29WB57PWSGczzZF9U80SPYPJNN1j3TIII9vxOXPes0fz2nnaC7qc0vuovKCz3IrMM9NKwGvdomWj328fw8JRnGPe+GdLwsFb09VIlqPF/imryp0xK9PWOJPa2dPr0FEwg8I8xtPVgVyTvreRS+E4zVPJRbqb05nji98Z96PWuRFzwBvJw8IWUmPAHX+LqDtFU9ExBuPSw1v7ziEyG8KGBRu75+9j2vpoq83uLhPEFHrT0cJ407rxPMu9VYo70znfe8Oz83u82V4T3gpIa7LHBlvUGohj0+x2c7tfOGO8rugb0yTLK9DxnVvKIf8r1pFDI9VZo+PFoztj0lL1k9DMPaO4yDdbzIjMG93ZfPO7vbyrzRZ328gbqQvMIvOj2t+Xc97Z3yPKaEpb137CK9T5JyvTpc172vIWW9w1gLPAaMJj1Ng6y9sr+APQhxwL3G/Y291KKtvIieBb1rRHS9veT1PeYRJj15XV29Tj66vf/DnD1qZwM+8q3dOz57K705ZpM9Sha7PXYGX7zI34G9L/AEPWK+0j3JYkA8N30ePVagQrz6FcS9n97cPYV7krz8FkE9P6zMvIhTnDuYGDy8bKyFveKODr6HrG88EDcfPCPAQb6s9p49xSgUval7oD22MLw8e2INPmC5Bb3VqWK8IZs5vWZKXb0GRiO9ggY0PXqjrj1/KqQ9CBU7vUbYYzsyeXI7ToIPPboCSzzUMxw9iSJpvQJ+3T1+GJo9pNmbPZ1cjj2XZJ293ls0PZsx+DyMs3+9A7UtPebfLr3nwwa+4CwVPDFHbL1apwG8y05wvUeOE77sP9S8siwZvcBObTxwtsg7zgWLPPL1yDsN9088k/ZTvBR6yrwdUL+96RmUPZOLrb35YV69yW0kvbmPfz3ehne9bGpevR3aFb6f38Y9XSVJPUpz/T1hEpA8wY3KPWmZgT3LM5Q9k6cWPF2OtDx8Ve89FvuvPSbPYz1itqs6wZ6xPcq+bL1C5t89Tb39PJB8lz3ikxK9hPkiPWadPj12kPE9hvsEvsp4AD2fCYg9d1DKPAnejrrafG49+EG2vWF/kb0UQZe8/RWEPTvbmzz9G3S8o7+OvDzJMz07cM+82piju9xAAz2UDoW9NxA0PADxEzz2p4o9/HuyvZbz6T3k4GM9/6y4vSqatbySAc+7Q2mSPf5m5zk2vMk9oQNQveGKdLz8mQA9Wi4cut/0Lz2w9gO92kCSvHkWLDzcTU69bQVsPbasPz7ypGI9NvM7PcHpVzxOWeA9iAJIvP+8ir0jEiK9s235PTMngb1w+6G9YwrkvVtAjjzYGoI87mulPSRhDzspY2C9G8KSvf1Wk7wrmgW8PrFovU2wqz1SXUM9v0sVPXi+Fr0SL8e9geScPXzf3bv20rE9TXwZPWV3OjxnjfK91TLKvUv9UT0NeFA8ekPZPE8w8z3REKE8ADyWPX0TXTpADgk9V0OAPfm5ub0eAH49kgK+ux+Vkr1BIsG9jK8sPYLS1LzOnsy9YPW0PTLt4r3aoYu5/Guavdggvj3Aywk8ucxMvTJJvD1d7Jo9tLUvPFii5D0pYYO9ZafNPRoejz0wAZS9+M9TvdXFcT0aZY899iTQPLD8rb2MC4g9OwpjvVYTMb2FoGG95o/1u7wOu7zmRgi+WuOfvfOl4byatKM94IHlPTv3VzwdVtY9mQ5mPQ9HV7wmX5C9KkE7PfiVxbrD7E08gxRXvD+0uzx+Ghu9/eMyva+vYL1MXjo9UrJ7veKqSrvFWBe9J7qZvOGgnT11plY8n1QpvQg3xT13Hzg9QdvaPVb0Lr08/rY8sK46vP00xLuW9E+6O1BpPN/+mD188EU8BWBnPWT/lr3E1gG9chvOPPjKgT07oMI8M0z+PUOz77xbuUq6fR0pu6ENJDyKeGA9IXFevQa2ITsgYF+9gxfXOyzOiTzXoPI801BfvY+wybtsRMY9LAVDvTz2oL0ypfU8sVy+Peql2DxoPAA+lGIaPutuRDtGx6W9xkJCPXDx6zxj+dO7jBhQvP5anLx0xp888DvkPGNzLj2t2EE9r5mbPcW5fr130Hi8x88xvcOjTbyh3jc+trCpPCyGwrxDq7W7/cNevXYelz1MWb48PfcBvB+6ZLzSRwg+la3RvRVoAz20F4U97Df3OrkwWD28jmc8Hz0+PTJKwLzYP+W86iJOPaSYaz2ahE+8+GgVvWCHOjyE5pk98eVhvAHay70xHA6+Xm4svL7ANb13ypK9cEKTvdcx0jwMl349FNBbvTjN2738djS9CoO5vS7D4zxZthG9QFrtPDGoajz4TU098mBHvQFihb324Bu9Pp3UPVvjsTzgv3w9GGaJvHgduj3CxXa7rKYVvWDvAr1+Zf89oLkdO6H/7byFsVg7Xk0ovhpoxrz7quO9e4QuvQic0bo53a8940YGPUtq5j2dXO08tlOfu63MmD3J4bc7SxOJvUlUyz3MiAC9vgGkPBCBK7xD6Lo9Xd1xPA==";
//				String token=r.getString("face_token");
//		String token1=get().getFaceToken("D:/1.jpg");
		String token1="AID6RAAAnEIV5Be98PM1PSAaiT3XXw69/zgzvUy4Er386zy9/B6MvIR1tTx2n0w9MYYxvXP6Br1JAwK+K6LoPKNzdT3D+e49ENcIvbuZWr01ZpS9HhM1u52Zhj08nju9pBXUPNUVVL2OcEU9aAqnPdO7Nb0FgoY8F+VaPPWSZ71djwe+y0ynPeQghj0eTIq9UZZnvR52Rbyx9qi9CO6VPReBND2u6sw9cyYQPAP0fr1AFwW9xbLPveiiHL2AcB29WB57PWSGczzZF9U80SPYPJNN1j3TIII9vxOXPes0fz2nnaC7qc0vuovKCz3IrMM9NKwGvdomWj328fw8JRnGPe+GdLwsFb09VIlqPF/imryp0xK9PWOJPa2dPr0FEwg8I8xtPVgVyTvreRS+E4zVPJRbqb05nji98Z96PWuRFzwBvJw8IWUmPAHX+LqDtFU9ExBuPSw1v7ziEyG8KGBRu75+9j2vpoq83uLhPEFHrT0cJ407rxPMu9VYo70znfe8Oz83u82V4T3gpIa7LHBlvUGohj0+x2c7tfOGO8rugb0yTLK9DxnVvKIf8r1pFDI9VZo+PFoztj0lL1k9DMPaO4yDdbzIjMG93ZfPO7vbyrzRZ328gbqQvMIvOj2t+Xc97Z3yPKaEpb137CK9T5JyvTpc172vIWW9w1gLPAaMJj1Ng6y9sr+APQhxwL3G/Y291KKtvIieBb1rRHS9veT1PeYRJj15XV29Tj66vf/DnD1qZwM+8q3dOz57K705ZpM9Sha7PXYGX7zI34G9L/AEPWK+0j3JYkA8N30ePVagQrz6FcS9n97cPYV7krz8FkE9P6zMvIhTnDuYGDy8bKyFveKODr6HrG88EDcfPCPAQb6s9p49xSgUval7oD22MLw8e2INPmC5Bb3VqWK8IZs5vWZKXb0GRiO9ggY0PXqjrj1/KqQ9CBU7vUbYYzsyeXI7ToIPPboCSzzUMxw9iSJpvQJ+3T1+GJo9pNmbPZ1cjj2XZJ293ls0PZsx+DyMs3+9A7UtPebfLr3nwwa+4CwVPDFHbL1apwG8y05wvUeOE77sP9S8siwZvcBObTxwtsg7zgWLPPL1yDsN9088k/ZTvBR6yrwdUL+96RmUPZOLrb35YV69yW0kvbmPfz3ehne9bGpevR3aFb6f38Y9XSVJPUpz/T1hEpA8wY3KPWmZgT3LM5Q9k6cWPF2OtDx8Ve89FvuvPSbPYz1itqs6wZ6xPcq+bL1C5t89Tb39PJB8lz3ikxK9hPkiPWadPj12kPE9hvsEvsp4AD2fCYg9d1DKPAnejrrafG49+EG2vWF/kb0UQZe8/RWEPTvbmzz9G3S8o7+OvDzJMz07cM+82piju9xAAz2UDoW9NxA0PADxEzz2p4o9/HuyvZbz6T3k4GM9/6y4vSqatbySAc+7Q2mSPf5m5zk2vMk9oQNQveGKdLz8mQA9Wi4cut/0Lz2w9gO92kCSvHkWLDzcTU69bQVsPbasPz7ypGI9NvM7PcHpVzxOWeA9iAJIvP+8ir0jEiK9s235PTMngb1w+6G9YwrkvVtAjjzYGoI87mulPSRhDzspY2C9G8KSvf1Wk7wrmgW8PrFovU2wqz1SXUM9v0sVPXi+Fr0SL8e9geScPXzf3bv20rE9TXwZPWV3OjxnjfK91TLKvUv9UT0NeFA8ekPZPE8w8z3REKE8ADyWPX0TXTpADgk9V0OAPfm5ub0eAH49kgK+ux+Vkr1BIsG9jK8sPYLS1LzOnsy9YPW0PTLt4r3aoYu5/Guavdggvj3Aywk8ucxMvTJJvD1d7Jo9tLUvPFii5D0pYYO9ZafNPRoejz0wAZS9+M9TvdXFcT0aZY899iTQPLD8rb2MC4g9OwpjvVYTMb2FoGG95o/1u7wOu7zmRgi+WuOfvfOl4byatKM94IHlPTv3VzwdVtY9mQ5mPQ9HV7wmX5C9KkE7PfiVxbrD7E08gxRXvD+0uzx+Ghu9/eMyva+vYL1MXjo9UrJ7veKqSrvFWBe9J7qZvOGgnT11plY8n1QpvQg3xT13Hzg9QdvaPVb0Lr08/rY8sK46vP00xLuW9E+6O1BpPN/+mD188EU8BWBnPWT/lr3E1gG9chvOPPjKgT07oMI8M0z+PUOz77xbuUq6fR0pu6ENJDyKeGA9IXFevQa2ITsgYF+9gxfXOyzOiTzXoPI801BfvY+wybtsRMY9LAVDvTz2oL0ypfU8sVy+Peql2DxoPAA+lGIaPutuRDtGx6W9xkJCPXDx6zxj+dO7jBhQvP5anLx0xp888DvkPGNzLj2t2EE9r5mbPcW5fr130Hi8x88xvcOjTbyh3jc+trCpPCyGwrxDq7W7/cNevXYelz1MWb48PfcBvB+6ZLzSRwg+la3RvRVoAz20F4U97Df3OrkwWD28jmc8Hz0+PTJKwLzYP+W86iJOPaSYaz2ahE+8+GgVvWCHOjyE5pk98eVhvAHay70xHA6+Xm4svL7ANb13ypK9cEKTvdcx0jwMl349FNBbvTjN2738djS9CoO5vS7D4zxZthG9QFrtPDGoajz4TU098mBHvQFihb324Bu9Pp3UPVvjsTzgv3w9GGaJvHgduj3CxXa7rKYVvWDvAr1+Zf89oLkdO6H/7byFsVg7Xk0ovhpoxrz7quO9e4QuvQic0bo53a8940YGPUtq5j2dXO08tlOfu63MmD3J4bc7SxOJvUlUyz3MiAC9vgGkPBCBK7xD6Lo9Xd1xPA==";
//		 //特征比对
        FaceFeature targetFaceFeature = new FaceFeature();
        byte[] bs=Base64.getDecoder().decode(token);
        System.err.println(""+bs[0]+bs[1]+bs[2]+bs[3]+bs[4]);
        targetFaceFeature.setFeatureData(Base64.getDecoder().decode(token));
        FaceFeature sourceFaceFeature = new FaceFeature();
        byte[] bs1=Base64.getDecoder().decode(token1);
        System.err.println(""+bs1[0]+bs1[1]+bs1[2]+bs1[3]+bs1[4]);
        sourceFaceFeature.setFeatureData(Base64.getDecoder().decode(token1));
        FaceSimilar faceSimilar = new FaceSimilar();
       int errorCode = get().faceEngine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
       System.out.println("特征比对errorCode:" + errorCode);
       System.out.println("人脸相似度：" + faceSimilar.getScore());
//		String a=Face.get().convertFileToBase64("c:/nginx/html/upload/ROOT/20210806/244264954163625984.jpg");
//		System.err.println(a.charAt(2));
	}

}