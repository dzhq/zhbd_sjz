//@json
function main(tbl){
	let cnd=Cnd.where("1","=",1);
	let pageNum=tbl.pageNum||1;
	let pageSize=tbl.pageSize||10;
	if(tbl.deptId){
		if(tbl.deptId!=1)
		cnd.and("dept_id","=",tbl.deptId)
	}
	if(tbl.username){
		cnd.and("username","=",tbl.username)
	}
	if(tbl.phone){
		cnd.and("phone","like",tbl.phone+"%")
	}
	if(tbl.status){
		cnd.and("status","=",tbl.status)
	}
	if(tbl.beginTime){
		if(tbl.endTime){
			cnd.and("date(add_time)","between",tbl.beginTime+","+tbl.endTime)
		}else
		cnd.and("add_time",">=",tbl.beginTime)
	}
	
	return dao.pager("sys_user",cnd,pageNum,pageSize)
}