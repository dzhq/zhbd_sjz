/**
 * iceCode v1.1.2
 * MIT License By www.iceui.net
 * 作者：ICE
 * ＱＱ：308018629
 * 官网：www.iceui.net
 * 说明：版权完全归iceui所有，转载和使用请注明版权
 */
;var ice = ice || {
	loadCss: function(url) {
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.href = url;
		head.appendChild(link);
	}
};
//模块链接地址
var moduleSrc = document.currentScript ? document.currentScript.src : document.scripts[document.scripts.length - 1].src;
//模块路径目录
var modulePath = moduleSrc.substring(0, moduleSrc.lastIndexOf('/')+1);
//加载css
ice.loadCss(modulePath+'iceCode.css');
//代码高亮
ice.highlight = function(data, language, setting) {
	language = language.toLowerCase() || 'js';
	//标签
	var LL = '|ICE_CODELIGHT_LL|';
	var LR = '|ICE_CODELIGHT_LR|';
	var RR = '|ICE_CODELIGHT_RR|';
	var PL = '|ICE_CODELIGHT_PL|';
	var PR = '|ICE_CODELIGHT_PR|';

	function html2text(html) {
		return html.replace(/[<>&"]/g, function(c) {
			return {
				'<': '&lt;',
				'>': '&gt;',
				'&': '&amp;',
				'"': '&quot;'
			}[c];
		});
	}

	function text2html(text) {
		var arrEntities = {
			'lt': '<',
			'gt': '>',
			'nbsp': ' ',
			'amp': '&',
			'quot': '"'
		};
		return text.replace(/&(lt|gt|nbsp|amp|quot);/ig, function(all, t) {
			return arrEntities[t];
		});
	}
	//通用高亮库
	//代码高亮规则，css为高亮的class，key为需要高亮的关键字，以空格分割，可以为正则表达式
	var light = {
		bracket: {
			css: 'bracket',
			key: '( ) [ ] { }',
		},
		operator: {
			css: 'operator',
			key: '+ - = * / %',
		},
		number: {
			css: 'number',
			key: /([\d]+)/g,
		},
		quotation: function(data) {
			//单引号与双引号
			var regs = new Array(/(\")([\d\D]*?)(\")/g, /(\')([\d\D]*?)(\')/g);
			for (var i = 0; i < regs.length; i++) {
				data = data.replace(regs[i], LL + 'quotation' + LR + '$1' + RR + LL + 'string' + LR + '$2' + RR + LL + 'quotation' + LR + '$3' + RR);
			}
			return data;
		},
		formatHtml: function(data) {
			return html2text(data);
		},
		formatText: function(data) {
			return text2html(data);
		},
		htmlComment: function(data) {
			//多行注释   <!-- …… -->
			return data.replace(/(\&lt;|<)!--(.|\n)*?--(\&gt;|>)/g, function(a) {
				a = a.split('\n');
				var html = '';
				for (var i = 0; i < a.length; i++) {
					a[i] = LL + 'comments' + LR + a[i] + RR;
				}
				return a.join('\n');
			});
		},
		xmlComment: function(data) {
			//多行注释   <![ …… [ …… ]]>
			return data.replace(/(\&lt;|<)!\[(\w|\s)*?\[(.|\s)*?\]\](\&gt;|>)/g, function(a) {
				return LL + 'comments' + LR + html2text(a) + RR;
			});
		},
		multiLineComment: function(data) {
			//多行注释   /* …… */
			return data.replace(/\/\*(.|\n)*?\*\//g, function(a) {
				a = a.split('\n');
				var html = '';
				for (var i = 0; i < a.length; i++) {
					a[i] = LL + 'comments' + LR + a[i] + RR;
				}
				return a.join('\n');
			});
		},
		singleLineComment: function(data) {
			//单行注释   // ……
			data = data.split('\n');
			for (var i = 0; i < data.length; i++) {
				if (data[i].indexOf('//') !== -1) {
					//判断双斜杠是否在引号里面
					var c = data[i].split('//');
					var t = '';
					var f = new Array();
					for (var d = 0; d < c.length - 1; d++) {
						f.push(c[d]);
						t += c[d];
						var as = t.split(LL).length - 1;
						var bs = t.split(RR).length - 1;
						if (as === bs) {
							var g = new Array();
							for (var e = d + 1; e < c.length; e++) {
								g.push(c[e]);
							}
							//清除里面的所有正则过的标签
							var k = g.join('//').replace(/\|ICE_CODELIGHT_LL\|.*?\|ICE_CODELIGHT_LR\|(.*?)\|ICE_CODELIGHT_RR\|/g, '$1');
							data[i] = f.join('//') + LL + 'comments' + LR + '//' + k + RR;
							break;
						}
					}
				}
			}
			return data.join('\n');
		},
	};
	//PHP高亮
	var light_PHP = {
		quotation: light.quotation,
		multiLineComment: light.multiLineComment,
		singleLineComment: light.singleLineComment,
		bracket: light.bracket,
		operator: light.operator,
		number: light.number,
		variable: {
			css: 'variable',
			key: /(\$\w+)/g,
		},
		functions: {
			css: 'functions',
			key: /\b(__halt_compiler|abstract|and|array|as|break|callable|case|catch|class|clone|const|continue|declare|default|die|do|echo|else|elseif|empty|enddeclare|endfor|endforeach|endif|endswitch|endwhile|eval|exit|extends|final|for|foreach|function|global|goto|if|implements|include|include_once|instanceof|insteadof|interface|isset|list|namespace|new|or|print|private|protected|public|require|require_once|return|static|switch|throw|trait|try|unset|use|var|while|xor)\b/g,
		},
	};
	//JS高亮
	var light_JS = {
		quotation: light.quotation,
		multiLineComment: light.multiLineComment,
		singleLineComment: light.singleLineComment,
		bracket: light.bracket,
		operator: light.operator,
		number: light.number,
		functions: {
			css: 'functions',
			key: /\b(break|delete|function|return|typeof|case|do|if|switch|var|catch|else|in|this|void|continue|false|instanceof|throw|while|debugger|finally|new|true|with|default|for|null|try)\b/g,
		},
	};
	//HTML高亮
	var light_HTML = {
		quotation: light.quotation,
		xmlComment: light.xmlComment,
		htmlComment: light.htmlComment,
		tag: function(data) {
			return data.replace(/<[^>]+>/gim, function(a) {
				//正则标签名称
				a = a.replace(/(<|<\/)([a-zA-Z]+)/ig, '$1' + LL + 'keyword' + LR + '$2' + RR);
				//正则标签内的attr属性名称
				return a.replace(/\s(((?!\s).)*?)\s?=+/gim, ' ' + LL + 'attr' + LR + '$1' + RR + '=');
			});
		},
		bracket: {
			css: 'bracket',
			key: '< >',
		},
		formatHtml: light.formatHtml,
	};

	var lightLanguage = {
		php: light_PHP,
		js: light_JS,
		html: light_HTML
	};

	if (typeof(setting) == 'object') {
		lightLanguage[language] = setting;
	}

	//高亮封装
	var run = {

		otherLight: function(code) {
			//单引号与双引号
			var regs = new Array(/(\")([\d\D]*?)(\")/g, /(\')([\d\D]*?)(\')/g);
			for (var i = 0; i < regs.length; i++) {
				code = code.replace(regs[i], LL + 'quotation' + LR + '$1' + RR + LL + 'string' + LR + '$2' + RR + LL + 'quotation' + LR + '$3' + RR);
			}

			//多行注释
			code = code.replace(/\/\*(.|\n)*?\*\//g, function(a) {
				a = a.split('\n');
				var html = '';
				for (var i = 0; i < a.length; i++) {
					a[i] = LL + 'comments' + LR + a[i] + RR;
				}
				return a.join('\n');
			});

			//单行注释
			code = code.split('\n');
			for (var i = 0; i < code.length; i++) {
				if (code[i].indexOf('//') !== -1) {
					//判断双斜杠是否在引号里面
					var c = code[i].split('//');
					var t = '';
					var f = new Array();
					for (var d = 0; d < c.length - 1; d++) {
						f.push(c[d]);
						t += c[d];
						var as = t.split(LL).length - 1;
						var bs = t.split(RR).length - 1;
						if (as === bs) {
							var g = new Array();
							for (var e = d + 1; e < c.length; e++) {
								g.push(c[e]);
							}
							//清除里面的所有正则过的标签
							var k = g.join('//').replace(/\|ICE_CODELIGHT_LL\|.*?\|ICE_CODELIGHT_LR\|(.*?)\|ICE_CODELIGHT_RR\|/g, '$1');
							code[i] = f.join('//') + LL + 'comments' + LR + '//' + k + RR;
							break;
						}
					}
				}
			}
			code = code.join('\n');
			return code;
		},

		//通过分割的方式替换
		mainLight: function(data, cond) {
			if (!cond.css) {
				return cond(data);
			}
			var keyArr = typeof(cond.key) != 'object' ? cond.key.split(' ') : 0;
			//选择一行一行的处理代码，防止出错
			data = data.split('\n');
			for (var i = 0; i < data.length; i++) {
				//如果关键字为正则的话，利用正则获取关键字
				if (typeof(cond.key) == 'object') keyArr = run.regex(data[i], cond.key);
				for (var e = 0; e < keyArr.length; e++) {

					//防止多余的空格存在
					if (!keyArr[e]) continue;
					var str = new Array();
					//处理
					if (data[i].indexOf(keyArr[e]) !== -1) {
						//判断双斜杠是否在引号里面
						var c = data[i].split(keyArr[e]);
						var t = '';
						for (var d = 0; d < c.length; d++) {
							str.push(c[d]);
							t += c[d];
							var as = t.split(LL).length - 1;
							var bs = t.split(RR).length - 1;
							if (as === bs) {
								str.push(LL + cond.css + LR + keyArr[e] + RR);
							} else {
								str.push(keyArr[e]);
							}
						}
						//将数组转为字符串
						var html = '';
						for (var a = 0; a < str.length - 1; a++) {
							html += str[a];
						}
						data[i] = html;
					}
				}
			}
			return data.join('\n');
		},

		//通过正则表达式替换
		regex: function(data, reg) {
			var arr = new Array();
			data.replace(reg, function(a, b, c) {
				arr.push(b);
			});
			return arr;
		},

		//运行高亮
		start: function(data) {
			//高亮代码
			//data = run.otherLight(data);
			for (k in lightLanguage[language]) {
				data = run.mainLight(data, lightLanguage[language][k]);
			}

			//释放真正的html标签
			return data.replace(/\|ICE_CODELIGHT_LL\|/g, '<span class="').replace(/\|ICE_CODELIGHT_LR\|/g, '">').replace(/\|ICE_CODELIGHT_RR\|/g, '</span>').replace(/\|ICE_CODELIGHT_PL\|/g, '<p>').replace(/\|ICE_CODELIGHT_PR\|/g, '</p>');
		}
	};

	var html = run.start(data).split('\n');
	//将html添加p标签
	for (var i = 0; i < html.length; i++) {
		html[i] = '<li><p>' + html[i] + '</p></li>';
	}
	language = (language == 'php' || language == 'js') ? 'default' : language;
	return {
		line: html.length,
		html: '<div class="code-main"><div class="code-line"></div><ul class="code-content code-' + language + '">' + html.join('') + '</ul></div>'
	};

};
/**
 * 代码展示
 * Date 2017-11-08
 */
ice.code = function(options) {
	options = options || {};
	var pre = options.pre.length > 0 ? options.pre[0] : options.pre; //对象
	var width = options.width || '100%'; //宽度
	var height = options.height || 'auto'; //高度
	var code = options.code || false; //代码
	var theme = options.theme || ''; //风格
	var hide = options.hide || false; //隐藏
	var light = options.light || false; //是否高亮
	var language = options.language || 'js'; //高亮语言
	var css = hide ? ' code-hide ' + theme : theme;
	var html = code ? code : pre.innerHTML;
	if (light) {
		html = ice.highlight(html, language, light);
	} else {
		var html = html.split('\n');
		//将html添加p标签
		for (var i = 0; i < html.length; i++) {
			html[i] = '<li><p>' + html[i].replace(/[<>&"]/g, function(c) {
				return {
					'<': '&lt;',
					'>': '&gt;',
					'&': '&amp;',
					'"': '&quot;'
				}[c];
			}) + '</p></li>';
		}
		html = {
			line: html.length,
			html: '<div class="code-main"><div class="code-line"></div><ul class="code-content">' + html.join('') + '</ul></div>'
		};
	}
	pre.innerHTML = '<div class="code-title">Code ' + language + '<span class="code-info">Line:' + html.line + '</span><span class="code-arrow"></span></div>' + html.html;
	pre.className = 'code' + css;
	pre.style.width = width;
	pre.style.height = height;
	pre.style.display = 'block';
	var box = pre.getElementsByClassName('code-main')[0];
	box.style.display = hide ? 'none' : 'block';
	pre.getElementsByClassName('code-arrow')[0].onclick = function() {
		togglecss(pre, 'code-hide');
		hascss(pre, 'code-hide') ? ice.slideup(box, 5) : ice.slidedown(box, 5);
	}
};