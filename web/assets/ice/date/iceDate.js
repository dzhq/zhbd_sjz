/**
 * iceDate v1.1.2
 * MIT License By www.iceui.net
 * 作者：ICE
 * ＱＱ：308018629
 * 邮箱：ice@iceui.net
 * 官网：www.iceui.net
 * 说明：版权完全归iceui所有，转载和使用请注明版权
 * -------------------------------------------------------------
 * 日历插件
 * Date 2019-01-05
 * options      {json}
 *  ├ id        {id}       input的id
 *  ├ format    {string}   时间格式
 *  ├ func      {function} 确认的回调函数
 *  ├ yearFunc  {function} 选择年份后的回调函数
 *  ├ monthFunc {function} 选择月份后的回调函数
 *  └ dayFunc   {function} 选择日期后的的回调函数
 * 说明：本插件返回的时间戳为10位，只精确到秒级，本座认为这只不过是个日历插件，毫秒毫无任何意义，所以……
 * 好吧，我承认我是个PHP爱好者，PHP的time()时间戳是10位，难道你让我在PHP中再次---时间戳/1000? FUCK,绝对不可能
 */
;var ice = ice || {
	loadCss: function(url) {
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.href = url;
		head.appendChild(link);
	}
};
//模块链接地址
var moduleSrc = document.currentScript ? document.currentScript.src : document.scripts[document.scripts.length - 1].src;
//模块路径目录
var modulePath = moduleSrc.substring(0, moduleSrc.lastIndexOf('/')+1);
//加载css
ice.loadCss(modulePath+'iceDate.css');
ice.date = function(options) {
	options = options || {};
	var id = options.id;
	var format = options.format || 'y-m-d';
	var func = options.func ||
	function() {};
	var yearFunc = options.yearFunc ||
	function() {};
	var monthFunc = options.monthFunc ||
	function() {};
	var dayFunc = options.dayFunc ||
	function() {};
	var obj = document.getElementById(id);
	var main = document.createElement('div');
	main.className = 'iceDate';
	main.style.zIndex = 9000;
	main.innerHTML = '<div class="iceDate-main"><div class="iceDate-header"><div class="iceDate-prev">〈</div><div class="iceDate-time"><div class="iceDate-time-year"></div><div class="iceDate-time-month"></div></div><div class="iceDate-next">〉</div></div><div class="iceDate-content"><div class="iceDate-box iceDate-year-box"></div><div class="iceDate-box iceDate-month-box"></div><div class="iceDate-box iceDate-day-box"><div class="iceDate-week"><span>日</span><span>一</span><span>二</span><span>三</span><span>四</span><span>五</span><span>六</span></div><div class="iceDate-day"></div></div><div class="iceDate-box iceDate-time-box"></div></div><div class="iceDate-footer"><div class="iceDate-footer-left"><span>选择时间</span></div><div class="iceDate-footer-right"><span>清空</span><span>现在</span><span>确定</span></div></div></div>';
	document.body.appendChild(main);
	//返回对象
	var c = function(a) {
			return main.getElementsByClassName(a)[0]
		}
		//获取对象
	var prev = c('iceDate-prev'); //上月
	var next = c('iceDate-next'); //下月
	var yearView = c('iceDate-time-year'); //年显示
	var monthView = c('iceDate-time-month'); //月显示
	var yearBox = c('iceDate-year-box'); //年选择器
	var monthBox = c('iceDate-month-box'); //月选择器
	var dayBox = c('iceDate-day-box'); //月选择器
	var dayView = c('iceDate-day'); //日历显示
	var timeBox = c('iceDate-time-box'); //时间选择器
	var footerBox = c('iceDate-footer'); //日历附加按钮
	var btnArr = footerBox.getElementsByTagName('span'); //按钮数组
	var boxArr = main.getElementsByClassName('iceDate-box'); //视图列表
	var timeBtn = btnArr[0]; //选择时间
	var emptyBtn = btnArr[1]; //清空
	var currentBtn = btnArr[2]; //现在
	var okBtn = btnArr[3]; //确认
	var hour, minute, second;
	//定义变量
	var newDate, y, m, d, h, i, s;
	//给input添加class
	if (obj.className.length) {
		var a = obj.className.split(' ');
		a.push('iceDate-icon');
		obj.className = a.join(' ');
	} else {
		obj.className = 'iceDate-icon';
	}
	//格式化input值
	if (obj.value.length) obj.value = formatDate(obj.value, format);
	//input焦点激活出发日历
	obj.onclick = function() {
		init(false, options.view ? options.view : 'day');
		main.style.display = 'block';
		main.style.zIndex = parseInt(main.style.zIndex) + 1;
	}
	//取消input焦点
	obj.onfocus = function() {
		this.blur()
	}
	//选择时间
	timeBtn.onclick = function() {
		if (timeBox.style.display == 'block') {
			viewActive('day');
		} else {
			viewActive('time');
			timeActive(h, i, s);
		}
	}
	//清空
	emptyBtn.onclick = function() {
		obj.value = '';
		init(new Date());
		main.style.display = 'none';
	}
	//现在
	currentBtn.onclick = function() {
		if (options.view) {
			newDate = new Date();
			if (options.view == 'year') {
				y = newDate.getFullYear();
				yearActive(y);
			} else if (options.view == 'month') {
				m = newDate.getMonth() + 1;
				monthActive(m);
			} else if (options.view == 'day') {
				d = newDate.getDate();
				dayActive(m);
			} else if (options.view == 'time') {
				h = newDate.getHours();
				i = newDate.getMinutes();
				s = newDate.getSeconds();
				timeActive(h, i, s);
			}
		} else {
			init(new Date());
		}
	}
	//确定
	okBtn.onclick = function() {
		viewActive('day');
		var time = y + '-' + m + '-' + d + ' ' + h + ':' + i + ':' + s;
		var date = new Date(y, m, d, h, i, s);
		var timestamp = date.getTime() / 1000;
		main.style.display = 'none';
		obj.value = formatDate(time, format);
		func({
			'elem': obj,
			'time': formatDate(time, format),
			'date': date,
			'timestamp': timestamp
		});
	}
	//年选择器
	yearView.onclick = function() {
		yearActive(y);
		viewActive('year');
	}
	//月选择器
	monthView.onclick = function() {
		monthActive(m);
		viewActive('month');
	}
	//上下翻月
	prev.onclick = function() {
		if (yearBox.style.display == 'block') {
			y -= 21;
			yearActive(y);
		} else if (monthBox.style.display == 'block') {
			if (m === 1) m = 13, --y;
			monthActive(--m);
		} else if (dayBox.style.display == 'block') {
			if (m === 1) m = 13, --y;
			dayActive(--m);
		}
	}
	next.onclick = function() {
		if (yearBox.style.display == 'block') {
			y += 21;
			yearActive(y);
		} else if (monthBox.style.display == 'block') {
			if (m === 12) m = 0, ++y;
			monthActive(++m);
		} else if (dayBox.style.display == 'block') {
			if (m === 12) m = 0, ++y;
			dayActive(++m);
		}
	}
	//将Date转化为指定格式的String->此方法已经封装在iceui框架的iceui.js模块中,操蛋，为了使本iceDate插件的独立性，不得不在这里再次粘贴一下

	function formatDate(param, format) {
		param = param || false;
		format = format || 'y-m-d h:i:s';
		if (param && new RegExp('(y|m|d|h|i|s)', 'i').test(param)) {
			var date = new Date();
			format = param;
		} else if (!param) {
			var date = new Date();
		} else {
			param = param.replace(/-/g, '/');
			if (parseInt(param) > 100000) { //如果为时间戳
				//php的时间戳是10位
				param = String(param).length === 10 ? parseInt(param + '000') : parseInt(param);
			}
			var date = new Date(param);
		}
		var o = {
			'm': '0' + (date.getMonth() + 1),
			//月
			'd': '0' + date.getDate(),
			//日
			'h': '0' + date.getHours(),
			//时
			'i': '0' + date.getMinutes(),
			//分
			's': '0' + date.getSeconds() //秒
		};
		format = format.replace(new RegExp('y', 'gi'), date.getFullYear()); //年
		for (var k in o) {
			o[k] = o[k].substring(o[k].length - 2, o[k].length);
			format = format.replace(new RegExp(k, 'gi'), o[k]);
		}
		return format;
	}
	//初始化

	function init(date, view) {
		var ys, ms, ds, hs, is, ss;
		if (options.view) {
			newDate = new Date();
			var v = obj.value.length ? parseInt(obj.value) : false;
			if (options.view == 'year') {
				ys = v ? v : new Date().getFullYear();
			} else if (options.view == 'month') {
				ms = v ? v : new Date().getMonth() + 1;
			} else if (options.view == 'day') {
				ds = v ? v : new Date().getDate();
			} else if (options.view == 'time') {
				v = obj.value.length ? obj.value : false;
				if (v) {
					v = v.split(':');
					hs = parseInt(v[0]);
					is = parseInt(v[1]);
					ss = parseInt(v[2]);
				}
			}
		} else {
			newDate = date ? date : obj.value.length ? new Date(obj.value.replace(/-/g, '/')) : new Date();
		}
		y = ys ? ys : newDate.getFullYear();
		m = ms ? ms : newDate.getMonth() + 1;
		d = ds ? ds : newDate.getDate();
		h = hs ? hs : newDate.getHours();
		i = is ? is : newDate.getMinutes();
		s = ss ? ss : newDate.getSeconds();
		yearActive(y); //激活年选择视图
		monthActive(m); //激活月选择视图
		dayActive(m); //激活日选择视图
		timeActive(h, i, s); //激活时间选择视图
		viewActive(view ? view : 'day'); //打开日选择视图
	}
	//激活视图

	function viewActive(box) {
		timeBtn.innerHTML = '选择时间';
		for (var a = 0, b; b = boxArr[a++];) {
			b.style.display = 'none';
		}
		if (box == 'year') {
			boxArr[0].style.display = 'block';
		} else if (box == 'month') {
			boxArr[1].style.display = 'block';
		} else if (box == 'day') {
			boxArr[2].style.display = 'block';
		} else if (box == 'time') {
			timeBtn.innerHTML = '返回日期';
			boxArr[3].style.display = 'block';
		}
	}
	//激活年选择视图

	function yearActive(nums) {
		var html = '';
		var old = nums - 10;
		for (var i = 0; i < 10; i++) {
			html += '<span>' + (old++) + '年</span>';
		}
		html += '<span class="iceDate-year-active">' + nums + '年</span>';
		for (var i = 0; i < 10; i++) {
			html += '<span>' + (++nums) + '年</span>';
		}
		yearBox.innerHTML = html;
		var span = yearBox.getElementsByTagName('span');
		//年选择器点击事件
		for (var i = 0, a; a = span[i++];) {
			a.onclick = function() {
				for (var ai = 0, as; as = span[ai++];) {
					as.className = '';
				}
				this.className = 'iceDate-year-active';
				yearView.innerHTML = this.innerHTML;
				y = parseInt(this.innerHTML.split('年')[0]);
				dayActive(m);
				var date = new Date(y, m, d);
				var timestamp = date.getTime() / 1000;
				yearFunc({
					'elem': obj,
					'time': y,
					'date': date,
					'timestamp': timestamp
				});
				if (options.view !== 'year') viewActive('day');
			}
		}
	}
	//激活月选择视图

	function monthActive(nums) {
		var html = '';
		for (var i = 1; i <= 12; i++) {
			html += i === nums ? '<span class="iceDate-month-active">' + i + '月</span>' : '<span>' + i + '月</span>';
		}
		monthView.innerHTML = nums + '月';
		monthBox.innerHTML = html;
		var span = monthBox.getElementsByTagName('span');
		//月选择器点击事件
		for (var i = 0, a; a = span[i++];) {
			a.onclick = function() {
				for (var ai = 0, as; as = span[ai++];) {
					as.className = '';
				}
				this.className = 'iceDate-month-active';
				monthView.innerHTML = this.innerHTML;
				m = parseInt(this.innerHTML.split('月')[0]);
				dayActive(m);
				var date = new Date(y, m, d);
				var timestamp = date.getTime() / 1000;
				monthFunc({
					'elem': obj,
					'time': m,
					'date': date,
					'timestamp': timestamp
				});
				if (options.view !== 'month') viewActive('day');
			}
		}
	}
	//激活日选择视图

	function dayActive(nums) {
		dayView.innerHTML = '';
		var activeDate = new Date(y, nums - 1, 1); //外面传进来的不断变化的日期对象
		var year = activeDate.getFullYear();
		var month = activeDate.getMonth(); //把当前的月份保存下来只是为了给title获取月份
		yearView.innerHTML = year + '年';
		monthView.innerHTML = month + 1 + '月';
		//创建日历上面的日期行数
		var n = 1 - activeDate.getDay();
		if (n == 1) n = -6;
		//为了日历更友好的显示三个月，让用户看的更明白。
		activeDate.setDate(n); //如果n为负数，则减少月份.在用这个月最后一天减去这个值就可以获得日历从哪天开始的。
		for (var i = 0; i < 42; i++) {
			var span = document.createElement('span');
			dayView.appendChild(span);
			var date = activeDate.getDate(); //返回日期1-31号
			span.innerHTML = date;
			if (date == d) span.className = 'iceDate-day-active';
			span.dateValue = year + "-" + (activeDate.getMonth() + 1) + "-" + date;
			span.onclick = function() {
				//obj.value = this.dateValue;//文本框获取的年月日
				var click = dayView.getElementsByClassName('iceDate-day-click');
				if (click.length > 0) click[0].className = '';
				if (this.className != 'iceDate-day-disabled' && this.className != 'iceDate-day-active') this.className = 'iceDate-day-click';
				//矫正日期
				d = parseInt(this.innerHTML);
				var date = new Date(y, m, d);
				var timestamp = date.getTime() / 1000;
				dayFunc({
					'elem': obj,
					'time': d,
					'date': date,
					'timestamp': timestamp
				});
			};
			if (activeDate.getMonth() != month) span.className = "iceDate-day-disabled";
			//如果超出该月份天数则增加月份
			activeDate.setDate(date + 1);
		}
	}
	//激活时间选择视图

	function timeActive(hNums, iNums, sNums) {
		hNums = parseInt(hNums), iNums = parseInt(iNums), sNums = parseInt(sNums);
		//时
		var hH = '';
		for (var a = 0; a < 24; a++) {
			var b = a < 10 ? '0' + a : a;
			hH += a === hNums ? '<span class="iceDate-time-active">' + b + '</span>' : '<span>' + b + '</span>';
		}
		//分
		var mH = '';
		for (var a = 0; a < 60; a++) {
			var b = a < 10 ? '0' + a : a;
			mH += a === iNums ? '<span class="iceDate-time-active">' + b + '</span>' : '<span>' + b + '</span>';
		}
		//秒
		var sH = '';
		for (var a = 0; a < 60; a++) {
			var b = a < 10 ? '0' + a : a;
			sH += a === sNums ? '<span class="iceDate-time-active">' + b + '</span>' : '<span>' + b + '</span>';
		}
		timeBox.innerHTML = '<div class="iceDate-time-text"><span>时</span><span>分</span><span>秒</span></div><div class="iceDate-time-main"><div class="iceDate-time-col"><div class="iceDate-hour">' + hH + '</div></div><div class="iceDate-time-col"><div class="iceDate-minute">' + mH + '</div></div><div class="iceDate-time-col"><div class="iceDate-second">' + sH + '</div></div></div>';
		hour = timeBox.getElementsByClassName('iceDate-hour')[0];
		minute = timeBox.getElementsByClassName('iceDate-minute')[0];
		second = timeBox.getElementsByClassName('iceDate-second')[0];
		var hourSpan = hour.getElementsByTagName('span');
		var minuteSpan = minute.getElementsByTagName('span');
		var secondSpan = second.getElementsByTagName('span');

		//不使用这种方法的话，运行过快会导致容器的offsetHeight一直为0，而设置scrollTop就会失效
		//所以判断offsetHeight是否大于0了，如果大于0说明文档已经加载完成，然后设置scrollTop
		//之前一直无法设置scrollTop还以为我的智商下线了，卧槽……尼玛的害得我检查了半天，竟然遇到这么一个「月经题」，操蛋
		var getH = setInterval(function() {
			if (hour.offsetHeight > 0) {
				//自动滚到时刻位置
				hour.scrollTop = (hNums - 3) * 32;
				minute.scrollTop = (iNums - 3) * 32;
				second.scrollTop = (sNums - 3) * 32;
				clearInterval(getH);
			}
		}, 0);

		//时选择器
		for (var a = 0; a < hourSpan.length; a++) {
			hourSpan[a].a = a;
			hourSpan[a].onclick = function() {
				for (var ai = 0, as; as = hourSpan[ai++];) {
					as.className = '';
				}
				this.className = 'iceDate-time-active';
				h = this.innerHTML;
				hour.scrollTop = (this.a - 3) * 32;
			}
		}
		//分选择器
		for (var a = 0; a < minuteSpan.length; a++) {
			minuteSpan[a].a = a;
			minuteSpan[a].onclick = function() {
				for (var ai = 0, as; as = minuteSpan[ai++];) {
					as.className = '';
				}
				this.className = 'iceDate-time-active';
				i = this.innerHTML;
				minute.scrollTop = (this.a - 3) * 32;
			}
		}
		//秒选择器
		for (var a = 0; a < secondSpan.length; a++) {
			secondSpan[a].a = a;
			secondSpan[a].onclick = function() {
				for (var ai = 0, as; as = secondSpan[ai++];) {
					as.className = '';
				}
				this.className = 'iceDate-time-active';
				s = this.innerHTML;
				second.scrollTop = (this.a - 3) * 32;
			}
		}
	}
};