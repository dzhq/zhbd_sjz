
# iceEditor富文本编辑器

#### 官方
+ iceCode 官方网站 [https://www.iceui.net/iceEditor.html](https://www.iceui.net/iceEditor.html)
+ iceCode 示例文档 [https://www.iceui.net/iceEditor/doc.html](https://www.iceui.net/iceEditor/doc.html)

#### 介绍
iceEditor是一款简约风格的富文本编辑器，体型十分娇小，无任何依赖，整个编辑器只有一个文件，功能却很不平凡！简约的唯美设计，简洁、极速、使用它的时候不需要引用jQuery、font、css……等文件，因为整个编辑器只是一个Js，支持上传图片、附件！支持添加音乐、视频！
iceEditor官方群：324415936

#### 优点
+ 纯原生开发，无任何依赖，冰清玉洁
+ 响应式布局，适应任何分辨率的设备
+ 整个编辑器只有一个文件，高效便捷
+ 简约的唯美设计，简洁、极速

#### 更新
+ **2020-04-20**
+ [新增] 添加视频解析功能，可直接添加b站和优酷的播放地址链接（其它视频平台将会陆续添加）
+ [优化] 编辑器样式，全部添加前缀，防止污染
+ [修复] 附件和图片上传失败所造成的BUG
+ [修复] upload.php文件返回json格式
+ **2020-04-17**
+ [修复] 复制粘贴被转义BUG
+ **2020-04-15**
+ [修复] Mac firefox下无法使用BUG
+ [修复] dome文件中A标签未闭合BUG
+ [修改] 默认取消编辑器的光标聚焦
+ [修改] 编辑器添加默认字体大小
+ **2020-04-10**
+ [修复] Windows firefox下无法使用BUG
+ [修复] uploadUrl没配置的情况下造成死循环BUG
+ [修复] 图片与附件上传关闭弹窗时造成打开其它页面BUG
+ [查看其它更新](https://www.iceui.net/iceEditor/update.html) 

#### 提示
[iceui](https://gitee.com/iceui/iceui) 前端框架已经已集成该编辑器。

#### 注意
iceEditor.js的引用禁止放在head标签内，请尽量放在body中或body后面！

#### 使用
```html
<div id="editor"> 欢迎使用iceEditor富文本编辑器 </div>
```
```javascript
//第一步：创建实例化对象
var e = new ice.editor('content');

//第二步：配置图片或附件上传提交表单的路径
//如果你的项目使用的php开发，可直接使用upload.php文件
//其它的编程语言需要你单独处理，后期我会放出.net java等语言代码
//具体与你平常处理multipart/form-data类型的表单一样
//唯一需要注意的就是：处理表单成功后要返回json格式字符串，不能包含其它多余的信息：
//url：文件的地址
//name：文件的名称（包含后缀）
//error：上传成功为0，其它为错误信息，将以弹窗形式提醒用户
//例如批量上传了两张图片：
//[
//	{url:/upload/img/153444.jpg, name:153444.jpg, error:0},
//	{url:/upload/img/153445.jpg, name:153445.jpg, error:'禁止该文件类型上传'}
//]
e.uploadUrl="/iceEditor/src/upload.php";

//第三步：配置菜单（默认加载全部，无需配置）
e.menu = [
  'backColor',                 //字体背景颜色
  'fontSize',                  //字体大小
  'foreColor',                 //字体颜色
  'bold',                      //粗体
  'italic',                    //斜体
  'underline',                 //下划线
  'strikeThrough',             //删除线
  'justifyLeft',               //左对齐
  'justifyCenter',             //居中对齐
  'justifyRight',              //右对齐
  'indent',                    //增加缩进
  'outdent',                   //减少缩进
  'insertOrderedList',         //有序列表
  'insertUnorderedList',       //无序列表
  'superscript',               //上标
  'subscript',                 //下标
  'createLink',                //创建连接
  'unlink',                    //取消连接
  'hr',                        //水平线
  'table',                     //表格
  'files',                     //附件
  'music',                     //音乐
  'video',                     //视频
  'insertImage',               //图片
  'removeFormat',              //格式化样式
  'code',                      //源码
  'line'                       //菜单分割线
];

//第四步：创建
e.create();
```

#### 设置编辑器尺寸
```javascript
var e = new ice.editor('content');
e.width='700px';   //宽度
e.height='300px';  //高度
e.create();
```

#### 禁用编辑器
```javascript
var e = new ice.editor('content');
e.disabled=true;
e.create();
```

#### 获取内容
```javascript
var e = new ice.editor('content');
console.log(e.getHTML());  //获取HTML格式内容
console.log(e.getText());  //获取Text格式内容
```

#### 设置内容
```javascript
var e = new ice.editor('content');
e.setValue('hello world！');
```

#### 追加内容
```javascript
var e = new ice.editor('content');
e.addValue('hello world！');
```