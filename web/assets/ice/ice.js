/**
 * ice v1.1.2
 * MIT License By www.iceui.net
 * 作者：ICE
 * ＱＱ：308018629
 * 官网：www.iceui.net
 * 说明：版权完全归iceui所有，转载和使用请注明版权
 */
;(function() {
	"use strict";
	console.time('ice');
	//实例化对象
	window.ice = function (str,obj){
		return new ice.init(str,obj);
	};
	//ice版本号
	ice.version = '1.1.2';
	//ice链接地址
	ice.src = document.currentScript ? document.currentScript.src : document.scripts[document.scripts.length - 1].src;
	//ice路径目录
	ice.path = ice.src.substring(0, ice.src.lastIndexOf('/')+1);
	//安装的模块列表
	ice.module = [];
	//内置可安装的模块列表
	ice.moduleList = {
		ui:{path:'ui/ui',expand:false},                //前端ui
		admin:{path:'admin/admin',expand:false},       //后台框架
		code:{path:'code/iceCode',expand:false},       //代码高亮
		date:{path:'date/iceDate',expand:false},       //时间控件
		editor:{path:'editor/iceEditor',expand:false}, //富文本编辑器
		drag:{path:'drag/drag',expand:false},          //拖拽排序
		popup:{path:'popup/popup',expand:false},       //弹出层
		tree:{path:'tree/tree',expand:false}           //树形控件
	};
	//生成模块绝对路径
	ice.modulePath = function(name) {
		if(!name) return false;
		//检测是否存在该模块
		if(!ice.inModule(name)) return;
		name = ice.moduleList[name].path + '.js';
		//如果路径第一个字符是/说明是根目录
		if(name[0] == '/') return location.origin + name;
		//默认为ice.js当前脚本所在目录
		return ice.path + name;
	};
	//判断模块是否存在
	ice.inModule = function(name){
		if(name.length && ice.moduleList[name] || ice[name]){
			return true;
		}else{
			console.error('ice：没有【'+name+'】这个模块');
			return false;
		}
	};
	//引入模块
	ice.inc = function(path,name){
		if(ice.inModule(name)){
			console.error('ice：【'+name+'】这个模块已存在，请重新命名');
			return false;
		}
		ice.moduleList[name] = {path:path,expand:true};
		return true;
	};
	//使用模块
	ice.use=function(name,callback){
		if(typeof name == 'string' && name.length>0){
			if(name.indexOf(' ') == -1){
				//检模块是否已经安装
				if(ice.module[name]){
					callback();
					return;
				}
				//检测是否存在该模块
				if(!ice.inModule(name)) return;
				//保存模块
				ice.module.push(name);
				//安装模块
				ice.loadJs(ice.modulePath(name),callback);
			}else{
				//利用递归方式完美保证顺序加载模块
				(function rec(url,i){
					i = i || 0;
					//检模块是否已经安装
					if(ice.module[url[i]]){
						callback();
						return;
					}
					//检测是否存在该模块
					if(!ice.inModule(url[i])) return;
					let u = ice.modulePath(url[i]);
					ice.loadJs(u,function(){
						i++;
						if(i<url.length){
							rec(url,i);
						}else{
							callback();
						}
					});
				})(name.split(' '));
			}
		}
	};
	/**
	 * 动态加载JS
	 * @param {string} url 脚本地址
	 * @param {function} callback  回调函数
	 * @return void
	 */
	ice.loadJs = function(url, callback) {
		if(!url) return false;
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		if (typeof(callback) == 'function') {
			script.onload = script.onreadystatechange = function() {
				if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
					callback();
					script.onload = script.onreadystatechange = null;
				}
			};
		}
		head.appendChild(script);
	};
	/**
	 * 动态加载CSS
	 * @param {string} url 样式地址
	 */
	ice.loadCss = function(url) {
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.href = url;
		head.appendChild(link);
	};
	//选择器
	ice.init = function(str, obj) {
		this.length = 0;
		obj = obj?obj:document;
		if (typeof str == 'string' && str.indexOf(' ') == -1) { //判断是否为组合类
			var a = str[0],
				b = str.slice(1); //获取属性名称
			if(obj.length){
				var arr = [];
				for(var i=0;i<obj.length;i++){
					if (a == '#') {
						return ice.elThat(this,document.getElementById(b));
					} else if (a == '.') {
						var el = obj[i].getElementsByClassName(b);
						if(el[0]){
							for(var s=0;s<el.length;s++){
								arr.push(el[s]);
							}
						}
					} else if (str.indexOf('[') == -1 && str.indexOf(':') == -1) {
						var el = obj[i].getElementsByTagName(str);
						if(el[0]){
							for(var s=0;s<el.length;s++){
								arr.push(el[s]);
							}
						}
					}
				}
				return ice.elThat(this,arr);
			}else{
				if (a == '#') {
					return ice.elThat(this,document.getElementById(b));
				} else if (a == '.') {
					return ice.elThat(this,obj.getElementsByClassName(b));
				} else if (str.indexOf('[') == -1 && str.indexOf(':') == -1) {
					return ice.elThat(this,obj.getElementsByTagName(str));
				}
			}
		}
		if (typeof str == 'object') return ice.elThat(this,str); //判断是否为对象
		if (typeof str == 'function') {
			ice.ready(str);
			return;
		}
		return ice.elThat(this,obj.querySelectorAll(str));
	};
	//继承方法
	ice.init.prototype={
		iceel:true,
		//遍历
		each: function(fn) {
			return ice.each(this, fn)
		},
		eq: function(s) {
			return ice(this[s]);
		},
		val: function(v){
			return ice.val(this,v);
		},
		//获取或设置html
		html: function(v) {
			return ice.html(this, v)
		},
		//打印对象
		log: function() {
			return ice.log(this)
		},
		//子元素的最后追加元素
		append: function(html) {
			return ice.append(this,html)
		},
		//子元素的前面追加元素
		prepend: function(html) {
			return ice.prepend(this,html)
		},
		//被选元素之前插入元素
		before: function(html) {
			return ice.before(this,html)
		},
		//被选元素之后插入元素
		after: function(html) {
			return ice.after(this,html)
		},
		//设置图片视图-水平垂直居中
		imgHVC: function(width, height) {
			return ice.imgHVC(this, width, height)
		},
		//获取对象距离窗口页面的顶部和左部的距离
		page: function() {
			return ice.page(this)
		},
		//复选框-全选
		checkall: function() {
			return ice.checkall(this)
		},
		//复选框-反选
		inverse: function() {
			return ice.inverse(this)
		},
		//获取css设置的样式属性
		getCss: function(attr) {
			return ice.getCss(this, attr)
		},
		//获取选择列表选中的值
		getSelect: function() {
			return ice.getSelect(this)
		},
		//删除对象
		del: function() {
			return ice.del(this)
		},
		//获取对象距离屏幕的偏移量
		offset: function() {
			return ice.offset(this)
		},
		//设置或获取对象style的属性值
		css: function(name, v) {
			return ice.css(this, name, v)
		},
		//设置或获取对象Attribute的属性值
		attr: function(name, v) {
			return ice.attr(this, name, v)
		},
		//删除attr
		delAttr: function(name) {
			return ice.delAttr(this, name)
		},
		//删除对象class
		delCss: function(name) {
			return ice.delCss(this, name)
		},
		//给对象添加点击事件
		click: function(fn) {
			return ice.click(this, fn)
		},
		//给对象添加点击事件
		dblclick: function(fn) {
			return ice.dblclick(this, fn)
		},
		change: function(fn) {
			return ice.change(this, fn)
		},
		//给对象添加事件
		on: function(type, fn, bool) {
			return ice.on(this, type, fn, bool)
		},
		//查找所有的祖先元素
		parents: function() {
			return ice.parents(this);
		},
		//给对象添加class
		addCss: function(name) {
			return ice.addCss(this, name)
		},
		//判断对象是否存在class
		hasCss: function(name) {
			return ice.hasCss(this, name)
		},
		//判断对象是否存在style
		hasStyle: function(name) {
			return ice.hasStyle(this, name)
		},
		//如果对象存在指定的css，则删除，不存在则创建
		toggleCss: function(nameA, nameB,fnA,fnB) {
			return ice.toggleCss(this, nameA, nameB,fnA,fnB)
		},
		//显示对象
		show: function() {
			return ice.show(this)
		},
		//隐藏对象
		hide: function() {
			return ice.hide(this)
		},
		//设置元素透明度
		opacity: function(s) {
			return ice.opacity(this, s)
		},
		//淡入效果
		fadeIn: function(speed, callback) {
			return ice.fadeIn(this, speed, callback)
		},
		//淡出效果
		fadeOut: function(speed, callback) {
			return ice.fadeOut(this, speed, callback)
		},
		//淡入出开关
		fadeToggle: function(speed, callback) {
			return ice.fadeToggle(this, speed, callback)
		},
		//向下滑动显示
		slideDown: function(time, callback) {
			return ice.slideDown(this, time, callback)
		},
		//向上滑动隐藏
		slideUp: function(time, callback) {
			return ice.slideUp(this, time, callback)
		},
		//向上滑动隐藏
		slideToggle: function(time, callback) {
			return ice.slideToggle(this, time, callback)
		},
		animate: function(json,options) {
			return ice.animate(this, json, options)
		},
	};
	//判断是否为function对象
	ice.isFunction = function(obj) {
		return typeof obj === "function" && typeof obj.nodeType !== "number";
	};
	//判断是否为window对象
	ice.isWindow = function isWindow(obj) {
		return obj != null && obj === obj.window;
	};
	//判断是否为array对象
	ice.isArrayLike = function(obj) {
		var length = !!obj && "length" in obj && obj.length,
			type = typeof obj;
		if (ice.isFunction(obj) || ice.isWindow(obj)) return false;
		return type === "array" || length === 0 || typeof length === "number" && length > 0 && (length - 1) in obj;
	};
	ice.elThat = function(that,el){
		if(!el || (typeof el == 'object' && el.length === 0) || (typeof el == 'array' && el.length === 0)){
			return that;
		}else{
			if(el.tagName == 'SELECT' || el.tagName == 'FORM'){
				that.length = 1;
				that[0] = el;
			}else if(el.length){
				that.length = el.length;
				let i = 0;
				for (; i < el.length; i++) {
					that[i] = el[i];
				}
			}else{
				that.length = 1;
				that[0] = el;
			}
		}
		return that;
	};
	//遍历
	ice.each = function(el, fn) {
		if(ice.isWindow(el)){
			fn.call(el, 0, el);
			return el;
		}
		if (!el) return false;
		var length, i = 0;
		if (ice.isArrayLike(el)) {
			length = el.length;
			for (; i < length; i++) {
				if (fn.call(el[i], i, el[i]) === false) break;
			}
		} else {
			for (i in el) {
				if (fn.call(el[i], i, el[i]) === false) break;
			}
		}
		return el;
	};
	//遍历全部节点
	ice.eachNode = function(el, fn, s) {
		s = s || 0;
		if (!el.children) return;
		for (var i = 0; i < el.children.length; i++) {
			s += i + 1;
			fn.call(el.children[i],s);
			if (el.children[i].children) ice.eachNode(el.children[i], fn, s);
		}
	};
	//字符串转dom节点
	ice.toDom = function (html) {
		if (typeof html === 'string') {
			var temp = document.createElement('div');
			temp.innerHTML = html;
			var frag = document.createDocumentFragment();
			while (temp.firstChild) {
				frag.appendChild(temp.firstChild);
			}
			return frag;
		} else {
			return html;
		}
	}
	//解析json
	ice.json_decode = function(str, bool) {
		if(!str || !str.length) return false;
		var obj = eval('(' + str + ')');
		if (bool) {
			var arr = []
			for (var i in obj) {
				arr.push(obj[i]);
			}
			return arr;
		}
		return obj;
	};
	//阻止冒泡
	ice.sp = function(event) {
		window.event ? window.event.cancelBubble = true : event.stopPropagation();
	};
	//阻止默认行为
	ice.pd = function(event) {
		window.event ? window.event.returnValue = false : event.preventDefault();
	};
	//注册滚动条监视事件
	ice.mouseWheel = function(obj, fn) {
		//判断鼠标滚轮滚动方向
		if (obj.addEventListener) obj.addEventListener('DOMMouseScroll', wheel, false);
		obj.onmousewheel = wheel; //W3C
		function stopDefault(e) {
			if (e && e.preventDefault) {
				e.preventDefault();
			} else {
				window.event.returnValue = false;
			}
			return false;
		}
		//统一处理滚轮滚动事件
		function wheel(event) {
			stopDefault(event);
			var delta = 0;
			if (!event) event = obj.event;
			if (event.wheelDelta) { //IE、chrome浏览器使用的是wheelDelta，并且值为“正负120”
				delta = event.wheelDelta / 120;
				if (obj.opera) delta = -delta; //因为IE、chrome等向下滚动是负值，FF是正值，为了处理一致性，在此取反处理
			} else if (event.detail) { //FF浏览器使用的是detail,其值为“正负3”
				delta = -event.detail / 3;
			}
			if (delta) {
				fn(delta);
			}
		}
	};
	//判断当前设备是否为移动端
	ice.isMobile = function() {
		return /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
	};
	//去掉字符串两端的空格 
	ice.trim = function(str) {
		if(!str.length) return '';
		return str.replace(/(^\s*)|(\s*$)/g, '');
	};
	//去掉所有的html标记
	ice.delTag = function(str) {
		if(!str.length) return '';
		return str.replace(/<[^>]+>/g, '');
	};
	//格式化字节单位
	ice.toSize = function(bytes) {
		if (bytes === 0) return '0 B';
		var k = 1024,
			sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
			i = Math.floor(Math.log(bytes) / Math.log(k));
		return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
	};
	//获取网址get的值
	ice.get = function(name, url) {
		url = url || 0;
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		var r = url ? url.search.substr(1).match(reg) : window.location.search.substr(1).match(reg);
		if (r != null) return decodeURI(r[2]);
		return null;
	};
	//当文档解析完毕且为操作准备就绪时，函数作为document的方法调用
	ice.ready = (function() {
		var f = [];
		var r = false;

		function handler(e) {
			if (r) return; //确保事件处理程序只完整运行一次
			if (e.type === 'onreadystatechange' && document.readyState !== 'complete') {
				return;
			}
			//运行所有注册函数，注意每次都要计算funcs.length
			//以防这些函数的调用可能会导致注册更多的函数
			for (var i = 0; i < f.length; i++) {
				f[i].call(document);
			}
			//事件处理函数完整执行,切换ready状态, 并移除所有函数
			r = true;
			f = null;
		}
		//为接收到的任何事件注册处理程序
		if (document.addEventListener) {
			document.addEventListener('DOMContentLoaded', handler, false);
			document.addEventListener('readystatechange', handler, false); //IE9+
			window.addEventListener('load', handler, false);
		} else if (document.attachEvent) {
			document.attachEvent('onreadystatechange', handler);
			window.attachEvent('onload', handler);
		}
		//返回ready()函数
		return function ready(fn) {
			if (r) {
				fn.call(document);
			} else {
				f.push(fn);
			}
		}
	})();
	ice.isMobile = function() {
		var mobileAgents = ['Android','iPhone','SymbianOS','WindowsPhone','iPad','iPod'];
		//根据userAgent判断是否是手机
		for (var v = 0; v < mobileAgents.length; v++) {
			if (navigator.userAgent.indexOf(mobileAgents[v]) > 0) {
				return true;
			}
		}
		//根据屏幕分辨率判断是否是手机
		if(window.screen.width < 500) return true;
		return false;
	};
	//一看就知道这是AJAX，省的我解释了，不然还得写一大溜参数说明
	ice.ajax = function(json) {
		json = json || {};
		if (!json.url) return;
		json.type = json.type || 'post';
		json.timeout = json.timeout || 15000;
		json.data = json.data || {};
		json.data.t = Math.random();
		json.json = json.json != undefined ? json.json : true;
		var json2url = function(json) {
			var arr = [];
			for (var name in json) {
				arr.push(name + '=' + encodeURIComponent(json[name]));
			}
			return arr.join('&');
		}
		//创建
		var xhr = new XMLHttpRequest();
		//连接 和 发送 - 第二步
		switch (json.type.toLowerCase()) {
			case 'get':
				xhr.open('GET', json.url + '?' + json2url(json.data), true);
				xhr.setRequestHeader('X-Requested-With','XMLHttpRequest'); 
				xhr.send();
				break;
			case 'post':
				xhr.open('POST', json.url, true);
				//设置表单提交时的内容类型
				xhr.setRequestHeader('X-Requested-With','XMLHttpRequest'); 
				xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xhr.send(json2url(json.data));
				break;
			default:
				xhr.open('GET', json.url + '?' + json2url(json.data), true);
				xhr.setRequestHeader('X-Requested-With','XMLHttpRequest'); 
				xhr.send();
				break;
		}
		//接收 - 第三步
		json.loading && json.loading();
		json.timer = setTimeout(function() {
			xhr.onreadystatechange = null;
			json.error && json.error('网络超时。');
		}, json.timeout);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				clearTimeout(json.timer);
				if (xhr.status >= 200 && xhr.status < 300 || xhr.status == 304) {
					if(xhr.responseText.length>0){
						var res = json.json ? JSON.parse(xhr.responseText) : xhr.responseText;
					}else{
						var res = '';
					}
					json.success && json.success(res);
				} else {
					json.error && json.error(xhr.status);
				}
			}
		};
	};
	//获取滚动条的偏移量
	ice.scroll = function(a) {
		var x = document.compatMode == 'CSS1Compat' ? (window.pageXOffset || document.documentElement.scrollLeft) :
			document.body.scrollLeft;
		var y = document.compatMode == 'CSS1Compat' ? (window.pageYOffset || document.documentElement.scrollTop) :
			document.body.scrollTop;
		if (a == 'x') {
			return x;
		} else if (a == 'y') {
			return y;
		} else if (!a) {
			return {
				'x': x,
				'y': y
			};
		}
		return obj;
	};
	//自动将滚动条至于最顶部
	ice.setScrollT = function() {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	};
	//自动将滚动条至于最底部
	ice.setScrollB = function() {
		document.body.scrollTop = document.documentElement.scrollTop = document.body.scrollHeight;
	};

	//页面视口的高宽
	ice.web = function() {
		var web = {};
		web.w = window.innerWidth;
		web.h = window.innerHeight; //页面视口高度
		if (typeof web.w != "number") {
			if (document.compatMode == "CSS1Compat") {
				web.w = document.documentElement.clientWidth;
				web.h = document.documentElement.clientHeight;
			} else {
				web.w = document.body.clientWidth;
				web.h = document.body.clientHeight;
			}
		}
		return web;
	};
	//获取随机数
	ice.getRandom = function(min, max) {
		return min + Math.round((max - min) * Math.random()); //四舍五入
	};
	//生成一个唯一的id
	ice.cid = function(str) {
		str = str || '_id';
		return str + new Date().getTime();
	};
	//跳转网页
	ice.jump = function(url) {
		location.href = url;
	};
	//子元素的最后追加元素
	ice.append = function(el,html) {
		el = ice.dom2arr(el);
		html = ice.toDom(html);
		return ice.each(el,function() {
			if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
				this.appendChild(html);
			}
		});
	};
	//子元素的前面追加元素
	ice.prepend = function(el,html) {
		el = ice.dom2arr(el);
		html = ice.toDom(html);
		return ice.each(el,function() {
			if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
				this.insertBefore(html,this.firstChild);
			}
		});
	};
	//被选元素之前插入元素
	ice.before = function(el,html) {
		el = ice.dom2arr(el);
		html = ice.toDom(html);
		return ice.each(el,function() {
			if (this.parentNode) {
				this.parentNode.insertBefore(html,this);
			}
		});
	};
	//被选元素之后插入元素
	ice.after = function(el,html) {
		el = ice.dom2arr(el);
		html = ice.toDom(html);
		return ice.each(el,function() {
			if (this.parentNode) {
				this.parentNode.insertBefore(html,this.nextSibling);
			}
		});
	};
	//显示对象
	ice.show = function(el) {
		el = ice.dom2arr(el);
		return ice.each(el, function() {
			this.style.display = '';
			let d = ice.getCss(this,'display');
			this.style.display = d == 'none'?'initial':d;
			if(ice.getCss(this,'visibility') == 'hidden') this.style.visibility = 'visible';
		});
	};
	//隐藏对象
	ice.hide = function(el) {
		el = ice.dom2arr(el);
		return ice.each(el, function() {
			this.style.display = 'none';
		});
	};
	//显示隐藏的元素 或 隐藏显示的元素
	ice.toggle = function(el) {
		el = ice.dom2arr(el);
		return ice.each(el, function() {
			if(ice.getCss(this,'display') == 'none' || ice.getCss(this,'visibility') == 'hidden'){
				ice.show(this);
			}else{
				ice.hide(this);
			}
		});
	};
	/**
	 * 设置元素透明度
	 * Date 2017-11-08
	 * el {object} 对象
	 * s  {Number} 透明值(0-100)
	 */
	ice.opacity = function(el, s) {
		el = ice.arr2dom(el);
		el.filters ? el.style.filter = 'alpha(opacity=' + s + ')' : el.style.opacity = s / 100;
	};
	/**
	 * 淡入效果
	 * Date 2017-11-08
	 * el       {object} 对象
	 * speed    {number} 淡入速度,(1秒=1000)
	 * opacity  {number} 淡入到指定透明值(0-100)
	 */
	ice.fadeIn = function(el, speed, callback) {
		el = ice.dom2arr(el);
		callback = callback || function(){};
		speed = speed===true?20:(speed?speed:20);
		return ice.each(el,function(a,b){
			var display = ice.attr(b,'ice-fade');
			if(display){
				b.style.display = display;
			}else{
				ice.setDisplay(b);
			}
			var s = 0;
			ice.opacity(b, s);
			+function t(){
				ice.opacity(b, s);
				s += 5;
				if (s <= 100) {
					setTimeout(t, speed);
				}else{
					callback.call(b, a, b);
				}
			}();
		});
	};
	/**
	 * 淡出效果
	 * Date 2017-11-08
	 * el       {object} 对象
	 * speed    {number} 淡出速度,(1秒=1000)
	 * callback {function} 回调函数
	 */
	ice.fadeOut = function(el, speed, callback) {
		el = ice.dom2arr(el);
		callback = callback || function(){};
		speed = speed===true?20:(speed?speed:20);
		return ice.each(el,function(a,b){
			ice.setDisplay(b);
			var s = this.style.opacity ? this.style.opacity * 100 : (this.filters ? this.filters.alpha.opacity : 100);
			var display = ice.getDisplay(b);
			(function t(){
				ice.opacity(b, s);
				s -= 5;
				if (s >= 0) {
					setTimeout(t, speed);
				} else if (s < 0) {
					b.style.display = 'none';
					ice.attr(b,'ice-fade',display);
					callback.call(b, a, b);
				}
			})();
		});
	};
	/**
	 * 淡入出效果
	 * Date 2017-11-08
	 * el       {object} 对象
	 * speed    {number} 淡出速度,(1秒=1000)
	 * callback {function} 回调函数
	 */
	ice.fadeToggle = function(el, speed, callback) {
		el = ice.dom2arr(el);
		callback = callback || function(){};
		speed = speed===true?20:(speed?speed:20);
		return ice.each(el,function(){
			if(ice.getCss(this,'display') == 'none'){
				ice.fadeIn(this, speed, callback);
			}else{
				ice.fadeOut(this, speed, callback);
			}
		});
	};
	//获取默认display
	ice.getDisplay = function(el){
		el = ice.arr2dom(el);
		if(el.style.display == 'none'){
			el.style.display = null;
			return ice.getCss(el,'display');
		}
		return ice.getCss(el,'display');
	};
	//设置默认display
	ice.setDisplay = function(el){
		el = ice.arr2dom(el);
		let display = ice.getCss(el,'display');
		if(display == 'inline'){
			el.style.display = 'inline-block';
		}else if(display != 'none'){
			el.style.display = display;
		}else{
			el.style.display = null;
			let display = ice.getCss(el,'display');
			if(display == 'inline'){
				el.style.display = 'inline-block';
			}else if(display == 'none'){
				el.style.display = 'block';
			}else{
				el.style.display = display;
			}
		}
	};
	//判断style是否存在某条属性
	ice.hasStyle = function(el,name){
		if(typeof el != 'string'){
			el = ice.arr2dom(el);
			var attr = ice.attr(el,'style');
		}else{
			var attr = el;
		}
		if(!attr) return false;
		//去掉所有空格
		attr = attr.replace(/\s/g,'');
		attr = attr.split(';');
		var i = attr.length;
		while(i--){
			if(attr[i].split(':')[0] == name) return true;
		}
		return false;
	};
	/**
	 * 向下滑动显示
	 * Date 2017-08-08
	 * el       {object} 对象
	 * speed     {number} 速度,(1秒=1000)
	 * callback {function} 回调函数
	 */
	ice.slideDown = function(el, speed, callback) {
		el = ice.dom2arr(el);
		callback = callback || function(){};
		speed = speed===true?10:(speed?speed:10);
		return ice.each(el,function(i,obj){
			if (this.style.display == 'block' || this.style.display == 'inline-block') return;
			//设置元素样式的本来dispaly
			ice.setDisplay(this);
			var isH = false,h=this.offsetHeight,a=0;
			if(this.style.height) isH = this.style.height;
			this.style.overflow = 'hidden';
			this.style.height = '0px';
			(function down() {
				if (a < h) {
					a+=5;
					obj.style.height = a + 'px';
					setTimeout(down, speed);
				} else {
					obj.style.overflow = null;
					obj.style.height = null;
					//还原之前的style样式
					ice.attr(obj,'style',ice.attr(obj,'ice-slide'));
					//删除备份的style样式
					ice.delAttr(obj,'ice-slide');
					//以防还原过来的style中的display为none
					ice.setDisplay(obj);
					callback.call(obj,i,obj);
				}
			})();
		});
	};
	/**
	 * 向上滑动隐藏
	 * Date 2017-08-08
	 * el    {object} 对象
	 * speed  {number} 速度,(1秒=1000)
	 * func  {function} 回调函数
	 */
	ice.slideUp = function(el, speed, callback) {
		el = ice.dom2arr(el);
		callback = callback || function(){};
		speed = speed===true?20:(speed?speed:20);
		return ice.each(el,function(i,obj){
			//备份style样式
			ice.attr(this,'ice-slide',ice.attr(this,'style'));
			if (this.style.display == 'none') return;
			//设置元素样式的本来dispaly
			ice.setDisplay(this);
			var isH = false,h=this.offsetHeight,a=h;
			if(this.style.height) isH = this.style.height;
			this.style.overflow = 'hidden';
			(function up() {
				if (a > 10) {
					a-=10;
					obj.style.height = a + 'px';
					setTimeout(up, speed);
				} else {
					obj.style.height = null;
					obj.style.display = 'none';
					obj.style.overflow = null;
					callback.call(obj,i,obj);
				}
			})();
		});
	};
	/**
	 * 上下滑动隐藏
	 * Date 2017-08-08
	 * el    {object} 对象
	 * speed  {number} 速度,(1秒=1000)
	 * func  {function} 回调函数
	 */
	ice.slideToggle = function(el, speed, callback) {
		el = ice.dom2arr(el);
		callback = callback || function(){};
		speed = speed===true?20:(speed?speed:20);
		return ice.each(el,function(){
			if(ice.getCss(this,'display') == 'none'){
				ice.slideDown(this,speed, callback);
			}else{
				ice.slideUp (this,speed, callback)
			}
		});
	};
	ice.animationFrame = function (callback) {
		var a=window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame,_this=this;
		(function b(){
			if(callback() !== false) a(b);
		})();
	};
	/**
	 * 动画
	 * obj: 运动对象
	 * json(json形式): 需要修改的属性
	 * options(json形式):
	 *   ├ duration: 运动时间
	 *   ├ easing: 运动方式（匀速、加速、减速）
	 *   └ complete: 运动完成后执行的函数
	 */
	ice.animate = function(el, json, options) {
		el = ice.dom2arr(el);
		return ice.each(el,function(){
			move(this, json, options);
		});
		function move(elem, jsonS, optionS) {
			optionS = optionS || {};
			var duration = optionS.duration || 500; //运动时间,默认值为500ms;
			var easing = optionS.easing || 'linear'; //运动方式,默认为linear匀速
			var start = {};
			var dis = {};
			var unit = {};
			for (var name in jsonS) {
				var n = parseInt(jsonS[name]);
				start[name] = parseFloat(ice.getCss(elem, name)); //起始位置
				dis[name] = n - start[name]; //总距离
				unit[name] = jsonS[name].slice(String(n).length);
			}
			var count = Math.floor(duration / 100 * 6); //动画1秒60帧，根据时间得出总次数
			var n = 0; //次数
			ice.animationFrame(function(){
				if (n > count) {
					optionS.complete && optionS.complete();
					return false;
				} else {
					for (var name in jsonS) {
						switch (easing) {
							//匀速
						case 'linear':
							var a = n / count;
							var cur = start[name] + dis[name] * a; //当前位置
							break;
							//加速
						case 'ease-in':
							var a = n / count;
							var cur = start[name] + dis[name] * a * a * a;
							break;
							//减速
						case 'ease-out':
							var a = 1 - n / count;
							var cur = start[name] + dis[name] * (1 - a * a * a);
							break;
						}
						if (name == 'opacity') {
							elem.style.opacity = cur;
							elem.style.filter = 'alpha(opacity=' + cur * 100 + ')'; //兼容IE8及以下
						} else {
							elem.style[name] = cur + unit[name];
						}
					}
				}
				n++;
			});
		};
	};
	//获取或设置html
	ice.html = function(el, v) {
		el = ice.dom2arr(el);
		if (v.length || v=='') {
			return ice.each(el,function(){
				this.innerHTML = v;
			});
		} else {
			return el[0]?el[0].innerHTML:el.innerHTML;
		}
	};
	//打印对象
	ice.log = function(el) {
		console.log(el);
		return el;
	};
	//设置图片视图-水平垂直居中
	ice.imgHVC = function(el, width, height) {
		el = ice.dom2arr(el);
		var img = new Image(),
			scale, newWidth, newHeight, div;
		return ice.each(el, function() {
			img.src = this.src;
			scale = Math.max(width / img.width, height / img.height);
			newWidth = img.width * scale;
			newHeight = img.height * scale;
			div = this.parentNode;
			this.width = newWidth;
			this.height = newHeight;
			div.style.width = width + 'px';
			div.style.height = height + 'px';
			div.style.overflow = 'hidden';
			div.style.display = 'inline-block';
			this.style.marginLeft = (width - newWidth) / 2 + 'px';
			this.style.marginTop = (height - newHeight) / 2 + 'px';
		});
	};
	//获取对象距离屏幕的偏移量
	ice.offset = function(el) {
		el = ice.arr2dom(el);
		var e = el,
			x = e.offsetLeft,
			y = e.offsetTop;
		while (e = e.offsetParent) {
			x += e.offsetLeft;
			y += e.offsetTop;
		}
		return {
			'left': x,
			'top': y
		};
	};
	//获取对象距离窗口页面的顶部和左部的距离
	ice.page = function(el) {
		el = ice.arr2dom(el);
		var box = el.getBoundingClientRect(),
			doc = el.ownerDocument,
			body = doc.body,
			html = doc.documentElement,
			clientTop = html.clientTop || body.clientTop || 0,
			clientLeft = html.clientLeft || body.clientLeft || 0,
			top = box.top + (self.pageYOffset || html.scrollTop || body.scrollTop) - clientTop,
			left = box.left + (self.pageXOffset || html.scrollLeft || body.scrollLeft) - clientLeft;
		return {
			'left': left,
			'top': top
		};
	};
	//复选框-全选
	ice.checkall = function(el) {
		el = ice.dom2arr(el);
		return ice.each(el, function() {
			this.checked = true
		});
	};
	//复选框-反选
	ice.inverse = function(el) {
		el = ice.dom2arr(el);
		return ice.each(el, function() {
			this.checked = this.checked?false:true;
		});
	};
	//获取css设置的样式属性
	ice.getCss = function(el, attr) {
		el = ice.arr2dom(el);
		return (el.currentStyle || getComputedStyle(el, false))[attr];
	};
	//删除对象
	ice.del = function(el) {
		el = ice.dom2arr(el);
		ice.each(el, function() {
			this.parentNode.removeChild(this)
		});
	};
	//获取选择列表选中的值
	ice.val = function(el,v) {
		el = ice.arr2dom(el);
		if(el.tagName == 'SELECT') {
			if(v != undefined){
				el.options[el.selectedIndex].value=v;
			}else{
				return el.options[el.selectedIndex].value;
			}
		}
		if(el.value != undefined) {
			if(v != undefined){
				el.value=v;
			}else{
				return el.value;
			}
			
		}
		return el;
	};
	//设置或获取对象style的属性值
	ice.css = function(el, name, value) {
		el = ice.dom2arr(el);
		if (arguments.length == 3 && value) {
			//设置一个样式
			return ice.each(el, function() {
				this.style[name] = value;
			});
		} else {
			if (typeof name == 'string') {
				//获取样式
				var that = el;
				return ice.each(el, function() {
					return that.getCss(this, name);
				});
			} else {
				//批量设置样式
				var json = name;
				return ice.each(el, function() {
					for (var name in json) {
						this.style[name] = json[name];
					}
				});
			}
		}
	};
	//设置或获取对象Attribute的属性值
	ice.attr = function(el, name, value) {
		el = ice.dom2arr(el);
		if (value) {
			//设置一个属性
			return ice.each(el, function() {
				this.setAttribute(name, value);
			});
		} else {
			if (typeof name == 'string') {
				//获取属性
				el = el[0]?el[0]:el;
				var a = el.getAttribute ? el.getAttribute(name) : false;
				return a==null?false:a;
			} else {
				//批量设置属性
				var json = name;
				return ice.each(el, function() {
					for (var name in json) {
						this.setAttribute(name, json[name]);
					}
				});
			}
		}
	};
	//删除attr
	ice.delAttr = function(el, name) {
		el = ice.dom2arr(el);
		return ice.each(el, function() {
			if (this.getAttribute(name)) this.removeAttribute(name);
		});
	};
	//删除对象class
	ice.delCss = function(el, name) {
		el = ice.dom2arr(el);
		function delcss(that,name){
			var re = new RegExp('\\b' + name + '\\b', 'g');
			if (that.className) {
				that.className = that.className.replace(re, '');
				that.className = that.className.replace(/^\s+|\s+$/g, '').replace(/\s+/g, ' ');
				if (that.className == '') {
					that.removeAttribute('class');
				}
			}
		}
		if(name.indexOf(' ') == -1){
			return ice.each(el, function() {
				delcss(this,name);
			});
		}else{
			name = name.split(' ');
			return ice.each(el, function() {
				for(var i=0;i<name.length;i++){
					delcss(this,name[i]);
				}
			});
		}
	};
	//给对象添加点击事件
	ice.click = function(el, fn) {
		el = ice.dom2arr(el);
		return ice.each(el, function(i, o) {
			o.onclick = function(e) {
				fn.call(o, i, o, e);
			}
		});
	};
	//给对象添加双击事件
	ice.dblclick = function(el, fn) {
		el = ice.dom2arr(el);
		return ice.each(el, function(i, o) {
			o.ondblclick = function(e) {
				fn.call(o, i, o, e);
			}
		});
	};
	//元素数据发生变化的时候触发
	ice.change = function(el, fn) {
		el = ice.dom2arr(el);
		return ice.each(el, function(i, o) {
			o.onchange = function(e) {
				fn.call(o, i, o, e);
			}
		});
	};
	//给对象添加事件
	ice.on = function(el, type, fn, bool) {
		if(!ice.isWindow(el))el = ice.dom2arr(el);
		bool = false || bool;
		return ice.each(el, function(i, o) {
			o.addEventListener(type, function(e) {
				fn.call(o, i, o, e);
			}, bool)
		});
	};
	//对象转为数组对象
	ice.dom2arr = function(el){
		if(!el || (typeof el == 'object' && el.length === 0) || (typeof el == 'array' && el.length === 0)){
			return false;
		}else{
			if(!el[0])el = [el];
		}
		return el;
	};
	//多个数组对象，转为第一个对象
	ice.arr2dom = function(el){
		if(!el || (typeof el == 'object' && el.length === 0) || (typeof el == 'array' && el.length === 0)){
			return false;
		}else{
			if(el[0])el = el[0];
		}
		return el;
	};
	//查找当前的直接祖先元素
	ice.parent = function(el) {
		return el.parentNode?el.parentNode:null;
	};
	//查找所有的祖先元素
	ice.parents = function(el) {
		var arr = [];
		el = ice.dom2arr(el);
		ice.each(el, function(i, o) {
			var p = this.parentNode;
			while (p !== document) {
				var o = p;
				arr.push(o);
				p = o.parentNode;
			}
		});
		return ice(arr);
	};
	//给对象添加class
	ice.addCss = function(el, name) {
		el = ice.dom2arr(el);
		function addcss(that,name){
			var re = new RegExp('\\b' + name + '\\b', 'g');
				if (that.className) {
				if (that.className.search(re) == -1) {
					that.className += ' ' + name;
				}
			} else {
				that.className = name;
			}
			that.className = that.className.replace(/^\s+|\s+$/g, '').replace(/\s+/g, ' ');
		}
		if(name.indexOf(' ') == -1){
			return ice.each(el, function() {
				addcss(this,name);
			});
		}else{
			name = name.split(' ');
			return ice.each(el, function() {
				for(var i=0;i<name.length;i++){
					addcss(this,name[i]);
				}
			});
		}
	};
	//判断对象是否存在class
	ice.hasCss = function(el, name) {
		el = ice.arr2dom(el);
		var css = el.className;
		if (!css) return false;
		css = css.split(' ');
		for (var i = 0;i<css.length;i++) {
			if (css[i] == name) return true;
		}
		return false;
	};
	//如果对象存在指定的css，则删除，不存在则创建
	ice.toggleCss = function(el, nameA, nameB,fnA,fnB) {
		fnA = fnA || function(){};
		fnB = fnB || function(){};
		el = ice.dom2arr(el);
		return ice.each(el, function(i,o) {
			if(nameB){
				if(ice.hasCss(this, nameA)){
					ice.delCss(this, nameA);
					ice.addCss(this, nameB);
					fnB.call(o, i, o);
				}else{
					ice.addCss(this, nameA);
					ice.delCss(this, nameB);
					fnA.call(o, i, o);
				}
			}else{
				ice.hasCss(this, nameA) ? ice.delCss(this, nameA) : ice.addCss(this, nameA);
			}
		});
	};
	/**
	 * 将Date转化为指定格式的String
	 * Date 2019-01-05
	 * param {string|int|empty} 时间戳|字符串类型时间戳|y-m-d格式|y-m-d格式的时间|为空将返回现在时间
	 * 例子：
	 * formatDate(1480385898)             返回时间戳时间：2016-11-29 10:18:18
	 * formatDate(1480385898,'y年m月d日') 返回时间戳时间：2016年11月29日
	 * formatDate()                       返回当前时间  ：2016-11-29 10:18:18(默认为y-m-d h:i:s格式)
	 * formatDate('y-m-d')                返回当前时间  ：2016-11-29
	 * formatDate('2016-11-29 10:18:18','y/m/d')   返回重新格式化后的时间  ：2016/11/29
	 * 说明：本函数的时间戳可以为10位或者13位，对于兼容10位因为对于时间的格式化，毫秒没有任何意义，只需要精确到秒即可
	 */
	ice.formatDate = function(param, format) {
		param = param || false;
		format = format || 'y-m-d h:i:s';
		if (param && new RegExp('(y|m|d|h|i|s)', 'i').test(param)) {
			var date = new Date();
			format = param;
		} else if (!param) {
			var date = new Date();
		} else {
			param = param.replace(/-/g, '/');
			if (parseInt(param) > 100000) { //如果为时间戳
				//php的时间戳是10位
				param = String(param).length === 10 ? parseInt(param + '000') : parseInt(param);
			}
			var date = new Date(param);
		}
		var o = {
			'm': '0' + (date.getMonth() + 1), //月
			'd': '0' + date.getDate(), //日
			'h': '0' + date.getHours(), //时
			'i': '0' + date.getMinutes(), //分
			's': '0' + date.getSeconds() //秒
		};
		format = format.replace(new RegExp('y', 'gi'), date.getFullYear()); //年
		for (var k in o) {
			o[k] = o[k].substring(o[k].length - 2, o[k].length);
			format = format.replace(new RegExp(k, 'gi'), o[k]);
		}
		return format;
	}
	/**
	 * 滚动条平滑滚动
	 * Date 2017-08-08
	 * el      {string} 存放锚点的容器#id或.class
	 * duration {number} 滚动速度
	 */
	ice.scrollAni = function(el, duration) {
		duration = duration || 200;
		var s = ice.scroll().y;
		//结果大于0,说明目标在下方,小于0,说明目标在上方
		var distance = ice.page(el).top - s;
		var scrollCount = duration / 10; //10毫秒滚动一次,计算滚动次数
		var everyDistance = distance / scrollCount //滚动距离除以滚动次数计算每次滚动距离
		for (var index = 1; index <= scrollCount; index++) {
			setTimeout(function() {
				window.scrollBy(0, everyDistance)
			}, 10 * index);
		}
	};
	/**
	 * 锚点平滑滚动
	 * Date 2017-08-08
	 * el      {string} 存放锚点的容器#id或.class
	 * duration {number} 滚动速度
	 */
	ice.scrollAnchor = function(el, duration) {
		duration = duration || 200;
		if (!el.length) return console.log('ice：scrollAnchor -->锚点的对象不能为空，请检查！');
		el.each(function(i,e){
			e.onclick = function() {
				let id = ice(this).attr('href');
				ice.scrollAni(ice(id), duration);
				return false;
			}
		});
	};
	/**
	 * 设置div滚动条
	 * Date 2019-1-10
	 * obj   {string} 需要设置滚动条的对象
	 * func  {function} 回调函数，传参为滚动条的状态值
	 */
	ice.scrollWheel = function(el, func) {
		func = func || false;
		var a = 0,
			i = 0,
			f = el.parentNode,
			//对象父元素
			vH = el.offsetHeight,
			//对象视口高度
			aH = el.scrollHeight; //对象文档高度
		//判断是否需要设置滚动条
		if (aH == vH) return;
		ice.addCss(f, 'scroll-active');

		//创建纵向滚动条box
		var scrollBox = document.createElement('div');
		scrollBox.className = 'scroll-y';

		//创建纵向滚动条滑动块
		var scrollMove = document.createElement('div');
		scrollMove.className = 'scroll-move-y';
		scrollMove.style.transition = 'none';
		scrollBox.appendChild(scrollMove);
		f.appendChild(scrollBox);
		var webH = ice.web().h;
		var moveH = parseInt(vH / (aH / vH));
		scrollMove.style.height = moveH + 'px';
		//获取滚动条与内容的像素比
		var scalePx = (aH - vH) / (vH - moveH);
		scrollMove.onmousedown = function(e) {
			var y = e.clientY - this.offsetTop;
			document.onmousemove = function(e) {
				i = e.clientY - y;
				if (i > vH - moveH) i = vH - moveH;
				if (i < 0) i = 0;
				el.scrollTop = i * scalePx;
				scrollMove.style.top = i + 'px';
			};
			document.onmouseup = function() {
				document.onmousemove = null;
				document.onmouseup = null;
			};
			return false;
		};

		//鼠标滚动
		mouseRun(el, func);

		function mouseRun(el, func) {
			//判断鼠标滚轮滚动方向
			if (el.addEventListener) el.addEventListener('DOMMouseScroll', wheel, false);
			el.onmousewheel = wheel; //W3C
			function stopDefault(e) {
				if (e && e.preventDefault) {
					e.preventDefault();
				} else {
					window.event.returnValue = false;
				}
				return false;
			}
			//统一处理滚轮滚动事件
			function wheel(event) {
				stopDefault(event);
				var delta = 0;
				if (!event) event = el.event;
				if (event.wheelDelta) { //IE、chrome浏览器使用的是wheelDelta，并且值为“正负120”
					delta = event.wheelDelta / 120;
					if (el.opera) delta = -delta; //因为IE、chrome等向下滚动是负值，FF是正值，为了处理一致性，在此取反处理
				} else if (event.detail) { //FF浏览器使用的是detail,其值为“正负3”
					delta = -event.detail / 3;
				}
				if (delta && a) {
					if (delta < 0) {
						i += 10;
						if (i > vH - moveH) i = vH - moveH;
					} else {
						i -= 10;
						if (i < 0) i = 0;
					}
					el.scrollTop = i * scalePx;
					scrollMove.style.top = i + 'px';
					if (func) func(i);
				}
			}
		}
		//添加事件
		el.onmouseover = function() {
			a = 1;
		}
		//移除事件
		el.onmouseout = function() {
			a = 0;
		}
	};
}());