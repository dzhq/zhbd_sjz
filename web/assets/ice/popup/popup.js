/**
 * 弹出层
 * Date 2017-08-07
 * 可自定义是否显示标题和页脚，并增加事件回调
 */
;var ice = ice || {};
ice.popup = function(options) {
	options = options || {};
	var width = options.width || 400; //默认宽度
	var height = options.height || 270; //默认高度
	var title = options.title || false; //默认不显示标题
	var content = options.content || '未定义'; //默认内容
	var time = options.time || false; //默认不自动关闭弹窗
	var background = options.background || false; //默认显示背景
	var callback = options.callback || false; //事件调用
	var drag = options.drag || false; //是否可拖拽
	var animate = options.animate || 'ani-up-in'; //弹窗的动画
	var position = options.position || 0; //弹窗的位置
	var color = options.color || 'white'; //弹窗的主题颜色
	var btn = options.btn || false;
	var yes = options.yes || false;
	var no = options.no || false;

	var webWidth = window.innerWidth;
	var webHeight = window.innerHeight;
	var getTime = new Date().getTime();
	var div_id = '_popup' + getTime;
	var div_drag = '_drag' + getTime;
	var div_close = '_close' + getTime;

	if (!document.getElementById('_popup')) {
		//创建父级div，方便管理所有弹窗
		var div = document.createElement('div');
		div.id = '_popup';
		document.body.appendChild(div);
		//创建子级div
		var div = document.getElementById('_popup');
		var p = document.createElement('div');
		p.id = div_id;
		div.appendChild(p);
	} else {
		//创建子级div
		var div = document.getElementById('_popup');
		var p = document.createElement('div');
		p.id = div_id;
		div.appendChild(p);
	}
	var popup = document.getElementById(div_id);

	//高度尺寸计算
	var content_css = btn ? ' popup-foot' : '';

	//创建按钮
	var btnHtml = '';
	if (btn) for (var i = 0, b; b = btn[i++];) {
		btnHtml += '<a href="javascript:;" class="popup-btn">' + b + '</a>';
	}

	//创建模型
	var html = '';
	if (background) {
		html += '<div class="popup-bg"></div>';
	}
	html += '<div id="' + div_drag + '" class="popup-box ' + animate + content_css + '" style="width:' + width + 'px;height:' + height + 'px;">';
	if (title) {
		html += '<div class="popup-header popup-' + color + '"><div class="popup-title">' + title + '</div><div class="popup-close"><a href="#" id="' + div_close + '">✕</a></div></div>';
	}
	html += '<div class="popup-content">' + content + '</div>';
	if (btn) {
		html += '<div class="popup-footer">' + btnHtml + '</div>';
	}
	html += '</div>';

	var close = function() {
			popup.parentNode.removeChild(popup);
		}
	popup.innerHTML = html;
	var obj = document.getElementById(div_drag);

	var btnObj = popup.getElementsByClassName('popup-btn');
	if (btnObj[0]) btnObj[0].onclick = function() {
		if (no)(no)();
		close();
	}
	if (btnObj[1]) btnObj[1].onclick = function() {
		if (yes)(yes)();
		close();
	}
	if (!btnObj[1] && btnObj[0]) btnObj[0].onclick = function() {
		if (yes)(yes)();
		close();
	}

	//弹窗对齐方式
	switch (position) {
	case 't':
		//顶部居中对齐
		obj.style.top = 0;
		obj.style.left = webWidth / 2 - width / 2 + 'px';
		obj.style.right = 'initial';
		obj.style.bottom = 'initial';
		break;
	case 'b':
		//底部居中对齐
		obj.style.bottom = 0;
		obj.style.left = webWidth / 2 - width / 2 + 'px';
		obj.style.right = 'initial';
		obj.style.top = 'initial';
		break;
	case 'l':
		//左边垂直对齐
		obj.style.left = 0;
		obj.style.top = webHeight / 2 - height / 2 + 'px';
		obj.style.right = 'initial';
		obj.style.bottom = 'initial';
		break;
	case 'r':
		//右边垂直对齐
		obj.style.right = 0;
		obj.style.top = webHeight / 2 - height / 2 + 'px';
		obj.style.left = 'initial';
		obj.style.bottom = 'initial';
		break;
	case 'lt':
		//左上角垂直对齐
		obj.style.left = 0;
		obj.style.top = 0;
		obj.style.right = 'initial';
		obj.style.bottom = 'initial';
		break;
	case 'lb':
		//左下角垂直对齐
		obj.style.left = 0;
		obj.style.bottom = 0;
		obj.style.right = 'initial';
		obj.style.top = 'initial';
		break;
	case 'rt':
		//右上角垂直对齐
		obj.style.right = 0;
		obj.style.top = 0;
		obj.style.left = 'initial';
		obj.style.bottom = 'initial';
		break;
	case 'rb':
		//右下角垂直对齐
		obj.style.right = 0;
		obj.style.bottom = 0;
		obj.style.left = 'initial';
		obj.style.top = 'initial';
		break;
	default:
		//默认垂直水平对齐
		break;
	}
	if (drag) {
		obj.onmousedown = function(ev) {
			var oEvent = ev || event,
				disX = oEvent.clientX - obj.offsetLeft,
				disY = oEvent.clientY - obj.offsetTop;
			document.onmousemove = function(ev) {
				oEvent = ev || event;
				obj.style.marginLeft = oEvent.clientX - disX + 'px';
				obj.style.marginTop = oEvent.clientY - disY + 'px';
			};
			document.onmouseup = function() {
				document.onmousemove = null;
				document.onmouseup = null;
			};
			return false;
		};
	}
	if (title) {
		document.getElementById(div_close).onclick = function() {
			close();
			return false;
		}
	}
	if (time) {
		setTimeout(close, time);
	}
};