/**
 * 树形菜单
 * Date 2017-05-02
 */
;var ice = ice || {};
ice.tree = function(options) {
	options = options || {};
	var div = options.div || 'tree'; //默认容器的id
	var json = options.json || 0; //传入的json数据，默认为空
	var id = options.id || 'id'; //json菜单的id，默认为id
	var pid = options.pid || 'pid'; //json菜单的pid，默认为pid
	var name = options.name || 'name'; //json菜单的name，默认为name
	var url = options.url || 'url'; //json菜单的url，默认为url
	var target = options.target || 0; //新窗口打开，默认为本窗口打开
	var allTarget = options.allTarget || 0; //全局新窗口打开，默认为本窗口打开
	var spread = options.spread || 0; //菜单展开，默认不展开全部菜单
	var trigger = options.trigger || 'click'; //触发方式，默认为鼠标点击弹出菜单
	var time = options.time || 10; //展开收缩动画速度，单位毫秒
	var func = options.func || false; //执行函数，展开收缩动画之前执行该函数
	var callback = options.callback || false; //回调函数，展开收缩动画之后执行该函数
	if (json) {
		//判断json的数据是否为字符串
		if (Object.prototype.toString.call(json) === "[object String]") {
			var list = eval("(" + json + ")");
		} else {
			var list = json;
		}

		//格式化数据，如果pid等于id，将pid的数据存入父级id的children中
		var data = new Array;
		for (var k in list) {
			data[list[k][id]] = list[k];
			data[list[k][id]].children = [];
		}
		for (var k in data) {
			if (data[k][pid] != 0) {
				data[data[k][pid]].children[k] = data[k];
			}
		}

		var num = 0;
		//无限递归函数，将所有的children取出来
		var listdata = function(json) {
				var html = '';
				while (true) {
					if (json.length > 0) {
						html += "<ul>";
						for (var k in json) {
							data[json[k][id]].order = num++; //给菜单添加序号
							target = allTarget ? '_blank' : (target ? json[k][target] : '_self');
							html += '<li><i class="iceui"></i><a href="' + json[k][url] + '" target="' + target + '">' + json[k][name] + '</a>';
							if (json[k].children.length > 0) {
								html += listdata(json[k].children);
							}
						}
						html += "</ul>";
						break;
					} else {
						break;
					}
				}
				return html;
			}

			//最终输出html格式数据
		var html = '';
		html += '<ul class="tree">';
		for (var k in data) {
			if (data[k][pid] == 0) {
				data[k].order = num++; //给菜单添加序号
				target = allTarget ? '_blank' : (target ? data[k][target] : '_self');
				html += '<li><i class="iceui"></i><a href="' + data[k][url] + '" target="' + target + '">' + data[k][name] + '</a>';
				html += listdata(data[k].children);
				html += '</li>';
			}
		}
		html += '</ul>';
		document.getElementById(div).innerHTML = html;
	}

	//将html数据二次处理，并添加样式和点击事件
	var p = document.getElementById(div).getElementsByTagName('a');
	for (var i = 0; i < p.length; i++) {
		if (json && func) {
			p[i].s = i;
			p[i].onclick = function() {
				for (var k in data) {
					if (data[k].order == this.s) {
						func(data[k]);
					}
				}
				return false;
			}
		}
		var ul = p[i].parentNode.getElementsByTagName('ul');
		if (ul.length) {
			if (spread) {
				ul[0].style.display = "block";
			} else {
				ul[0].style.display = "none";
			}

			if (ice.hasCss(p[i], 'open')) {
				ul[0].style.display = "block";
				p[i].className = "open";
			} else if (ice.hasCss(p[i], 'close')) {
				ul[0].style.display = "none";
				p[i].className = "close";
			} else {
				if (spread) {
					ul[0].style.display = "block";
					p[i].className = "open";
				} else {
					ul[0].style.display = "none";
					p[i].className = "close";
				}
			}

			p[i].i = ul[0];
			//给拥有下级菜单的菜单添加点击弹出事件
			if (trigger == 'click') {
				p[i].onclick = function() {
					if (func) {
						func(this);
					}
					if (this.i.style.display == "none") {
						ice.slideDown(this.i, time);
						this.className = "open";
					} else {
						ice.slideUp(this.i, time);
						this.className = "close";
					}
					return false;
				}
			} else if (trigger == 'hover') {
				p[i].onmouseover = function() {
					ice.slideDown(this.i, time);
					this.className = "open";
					return false;
				}
				p[i].onmouseout = function() {
					ice.slideUp(this.i, time);
					this.className = "close";
					return false;
				}
			} else {
				console.error('tree.js : trigger调用错误，只能为click或hover，默认为click');
			}
		}else{
			p[i].className = "alone";
		}
	}
};