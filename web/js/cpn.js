Vue.component("Pagination", {
    name: 'Pagination',
    template: `
    <div :class="{'hidden':hidden}" class="pagination-container"> 
    <el-pagination
      :background="background"
      :current-page.sync="currentPage"
      :page-size.sync="pageSize"
      :layout="layout"
      :page-sizes="pageSizes"
      :pager-count="pagerCount"
      :total="total"
      v-bind="$attrs"
      @size-change="handleSizeChange"
      @current-change="handleCurrentChange"
    />
  </div>
    `,
    props: {
        total: {
            required: true,
            type: Number
        },
        page: {
            type: Number,
            default: 1
        },
        limit: {
            type: Number,
            default: 20
        },
        pageSizes: {
            type: Array,
            default: function _default() {
                return [10, 20, 30, 50];
            }
        },
        // 移动端页码按钮的数量端默认值5
        pagerCount: {
            type: Number,
            default: document.body.clientWidth < 992 ? 5 : 7
        },
        layout: {
            type: String,
            default: 'total, sizes, prev, pager, next, jumper'
        },
        background: {
            type: Boolean,
            default: true
        },
        autoScroll: {
            type: Boolean,
            default: true
        },
        hidden: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        currentPage: {
            get: function get() {
                return this.page;
            },
            set: function set(val) {
                this.$emit('update:page', val);
            }
        },
        pageSize: {
            get: function get() {
                return this.limit;
            },
            set: function set(val) {
                this.$emit('update:limit', val);
            }
        }
    },
    methods: {
        handleSizeChange: function handleSizeChange(val) {
            this.$emit('pagination', {
                page: this.currentPage,
                limit: val
            });

        },
        handleCurrentChange: function handleCurrentChange(val) {
            this.$emit('pagination', {
                page: val,
                limit: this.pageSize
            });

        }
    }
})



Vue.component("Upload", {
    template: `
    <div class="upload-file">
    <el-upload
      :action="uploadFileUrl"
      :before-upload="handleBeforeUpload"
      :file-list="fileList"
      :limit="1"
      :on-error="handleUploadError"
      :on-exceed="handleExceed"
      :on-success="handleUploadSuccess"
      :show-file-list="false"
      style="margin-bottom: 5px;"
      ref="upload"
    >
      <!-- 上传按钮 -->
      <el-button size="mini" type="primary">{{text || '导入'}}</el-button>
      <!-- 上传提示 -->
      <div class="el-upload__tip" slot="tip" v-if="showTip">
        请上传
        <template v-if="fileSize"> 大小不超过 <b style="color: #f56c6c">{{ fileSize }}MB</b> </template>
        <template v-if="fileType"> 格式为 <b style="color: #f56c6c">{{ fileType.join("/") }}</b> </template>
        的文件
      </div>
    </el-upload>

    <!-- 文件列表 -->
    <transition-group class="upload-file-list el-upload-list el-upload-list--text" name="el-fade-in-linear" tag="ul">
      <li :key="file.uid" class="el-upload-list__item ele-upload-list__item-content" v-for="(file, index) in list">
        <el-link :href="file.url" :underline="false" target="_blank">
          <span class="el-icon-document"> {{ getFileName(file.name) }} </span>
        </el-link>
        <div class="ele-upload-list__item-content-action">
          <el-link :underline="false" @click="handleDelete(index)" type="danger">删除</el-link>
        </div>
      </li>
    </transition-group>
  </div>
    `,
    props: {
        // 值
        value: [String, Object, Array],
        // 大小限制(MB)
        fileSize: {
            type: Number,
            default: 5,
        },
        // 文件类型, 例如['png', 'jpg', 'jpeg']
        fileType: {
            type: Array,
            default: () => ["doc", "xls", "ppt", "txt", "pdf"],
        },
        // 是否显示提示
        isShowTip: {
            type: Boolean,
            default: true
        },
        text: {
            type: String,
            default: ''
        }
    },
    data() {
        return {
            uploadFileUrl:"", // 上传的图片服务器地址
            fileList: [],
        };
    },
    computed: {
        // 是否显示提示
        showTip() {
            return this.isShowTip && (this.fileType || this.fileSize);
        },
        // 列表
        list() {
            let temp = 1;
            if (this.value) {
                // 首先将值转为数组
                const list = Array.isArray(this.value) ? this.value : [this.value];
                // 然后将数组转为对象数组
                return list.map((item) => {
                    if (typeof item === "string") {
                        item = { name: item, url: item };
                    }
                    item.uid = item.uid || new Date().getTime() + temp++;
                    return item;
                });
            } else {
                this.fileList = [];
                return [];
            }
        },
    },
    methods: {
        // 上传前校检格式和大小
        handleBeforeUpload(file) {
            // 校检文件类型
            if (this.fileType) {
                let fileExtension = "";
                if (file.name.lastIndexOf(".") > -1) {
                    fileExtension = file.name.slice(file.name.lastIndexOf(".") + 1);
                }
                const isTypeOk = this.fileType.some((type) => {
                    if (file.type.indexOf(type) > -1) return true;
                    if (fileExtension && fileExtension.indexOf(type) > -1) return true;
                    return false;
                });
                if (!isTypeOk) {
                    this.$message.error(`文件格式不正确, 请上传${this.fileType.join("/")}格式文件!`);
                    return false;
                }
            }
            // 校检文件大小
            if (this.fileSize) {
                const isLt = file.size / 1024 / 1024 < this.fileSize;
                if (!isLt) {
                    this.$message.error(`上传文件大小不能超过 ${this.fileSize} MB!`);
                    return false;
                }
            }
            return true;
        },
        // 文件个数超出
        handleExceed() {
            this.$message.error(`只允许上传单个文件`);
        },
        // 上传失败
        handleUploadError(err) {
            this.$message.error("上传失败, 请重试");
        },
        // 上传成功回调
        handleUploadSuccess(res, file) {
            this.$message.success("上传成功");
            this.$emit("input", res.url);
        },
        // 删除文件
        handleDelete(index) {
            this.fileList.splice(index, 1);
            this.$emit("input", '');
        },
        // 获取文件名称
        getFileName(name) {
            if (name.lastIndexOf("/") > -1) {
                return name.slice(name.lastIndexOf("/") + 1).toLowerCase();
            } else {
                return "";
            }
        }
    },
    created() {
        this.fileList = this.list;
    }
})