//三链图
        // 路径配置
       
        var nodes = [];
	var links = [];
	var constMaxDepth = 2;
	var constMaxChildren = 7;
	var constMinChildren = 4;
	var constMaxRadius = 10;
	var constMinRadius = 2;
	
nodes = [//展示的节点  
        {  
            "name": "齐悦科技",//节点名称  
            "value": 43,
            "depth": 0,
            "id": 0,
            "category": 3//与关系网类别索引对应，此处只有一个关系网所以这里写0  
        },  
        {  
            "name": "股权链",  
            "value": 20,  
            "category": 2,
            "depth": 1,
            "id": 1,  
        },  
        {  
            "name": "信用链",  
            "value": 20,  
            "category": 2,
            "depth": 2,
            "id": 2, 
        },
       {  
            "name": "股东",  
            "value": 10,  
            "category": 1 ,
            "depth": 1,
            "id": 3
        },{  
            "name": "愉悦投资66.67%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 4
        },{  
            "name": "愉悦投资33.33%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 5
        },{  
            "name": "供应链",  
            "value": 20,  
            "category": 2,
            "depth": 3,
            "id": 6, 
        }, 
           {  
            "name": "控股公司",  
            "value": 10,  
            "category": 1 ,
            "depth": 1,
            "id": 7
        },{  
            "name": "愉悦家纺100%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 8
        },{  
            "name": "东力热电45%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 9
        },{  
            "name": "愉悦物流60%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 10
        },{  
            "name": "居家悦品90%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 11
        },{  
            "name": "黄河三角洲19.29%",  
            "value": 3,  
            "category": 0 ,
            "depth": 1,
            "id": 12
        },{  
            "name": "关联公司",  
            "value": 10,  
            "category": 1 ,
            "depth": 2,
            "id": 13
        }
        ,{  
            "name": "银行借款",  
            "value": 10,  
            "category": 1 ,
            "depth": 2,
            "id": 14
        },{  
            "name": "银行授信",  
            "value": 10,  
            "category": 1 ,
            "depth": 2,
            "id": 15
        },{  
            "name": "存续债券",  
            "value": 10,  
            "category": 1 ,
            "depth": 2,
            "id": 16
        },{  
            "name": "担保类系链",  
            "value": 10,  
            "category": 1 ,
            "depth": 2,
            "id": 17
        },{  
            "name": "滨印集团28%",  
            "value": 3,  
            "category": 0 ,
            "depth": 2,
            "id": 18
        },{  
            "name": "17齐悦0183%",  
            "value": 3,  
            "category": 0 ,
            "depth": 2,
            "id": 19
        },{  
            "name": "上游",  
            "value": 10,  
            "category": 1 ,
            "depth": 3,
            "id": 20
        },{  
            "name": "下游",  
            "value": 10,  
            "category": 1 ,
            "depth": 3,
            "id": 21
        },{  
            "name": "铜陵华源麻业9.86%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 22
        },{  
            "name": "邹平泰升纺织7.31%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 23
        },{  
            "name": "荆州福瑞源纺织6.93%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 24
        },{  
            "name": "山东祥瑞祥纺织6.55%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 25
        },{  
            "name": "江苏明源纺织6.31%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 26
        },{  
            "name": "IKEA Supply AG18.45%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 27
        },{  
            "name": "威海海思5.74%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 28
        },{  
            "name": "圣美伦5.62%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 29
        },{  
            "name": "威海润凡4.16%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 30
        },{  
            "name": "宜家分拨（上海）3.79%",  
            "value": 3,  
            "category": 0 ,
            "depth": 3,
            "id": 31
        }
    ];
    links =  [//节点之间连接  
        {  
            "source": 0,//起始节点，0表示第一个节点  
            "target": 1,
            "weight": 1
        },
        {  
            "source": 0,  
            "target": 2  
        },
        {  
            "source": 0,//起始节点，0表示第一个节点  
            "target": 6,
            "weight": 1
        },{
            "source": 1,  
            "target": 3  
        },{
            "source": 1,  
            "target": 7  
        },{  
            "source": 3,  
            "target": 4  
        }
        ,{  
            "source": 3,  
            "target": 5  
        },{
            "source": 7,  
            "target": 8  
        },{
            "source": 7,  
            "target": 9  
        },
        {
            "source": 7,  
            "target": 10 
        },{
            "source": 7,  
            "target": 11
        },{
            "source": 7,  
            "target": 12 
        },{
            "source": 2,  
            "target": 13 
        },{
            "source": 2,  
            "target": 14 
        },{
            "source": 2,  
            "target": 15 
        },{
            "source": 2,  
            "target": 16 
        },{
            "source": 2,  
            "target": 17 
        },{
            "source": 17,  
            "target": 18 
        },{
            "source": 16,  
            "target": 19 
        },{
            "source": 6,  
            "target": 20 
        },{
            "source": 6,  
            "target": 21 
        },{
            "source": 20,  
            "target": 22 
        },{
            "source": 20,  
            "target": 23
        },{
            "source": 20,  
            "target": 24 
        },{
            "source": 20,  
            "target": 25
        },{
            "source": 20,  
            "target": 26 
        },{
            "source": 21,  
            "target": 27 
        },{
            "source": 21,  
            "target": 28 
        },{
            "source": 21,  
            "target": 29 
        },{
            "source": 21,  
            "target": 30 
        },{
            "source": 21,  
            "target": 31 
        },{
            "source": 21,  
            "target": 32 
        }
    ] 
    for(var i=0; i<nodes.length; i++){
    	if(nodes[i].id == "0"){
    		nodes[i].itemStyle = {
    			normal: {
    				borderColor: '#000',
    				color:'blue',
    				label: {
		                show: true,
		            }
				}
    		}
    	}
    	else if(nodes[i].id == "22"){
    		nodes[i].itemStyle = {
    			normal: {
    				label: {
		                show: true,
		                textStyle:{
		                	color:'yellow'
		                }
		                
		            },
    			}
    		}
    	}
    	//22
    	nodes[i]["symbolSize"] = nodes[i].value *1.2;
    }

	        // 基于准备好的dom，初始化echarts图表
	        var guanxi = echarts.init(document.getElementById('guanxi')); 
			option3 ={
    title: {
        text: '自定义雷达图'
    },
    legend: {
        data: ['图一','图二', '张三', '李四']
    },
    radar: [
        {
            indicator: [
                { text: '指标一' },
                { text: '指标二' },
                { text: '指标三' },
                { text: '指标四' },
                { text: '指标五' }
            ],
            center: ['25%', '50%'],
            radius: 120,
            startAngle: 90,
            splitNumber: 4,
            shape: 'circle',
            name: {
                formatter: '【{value}】',
                textStyle: {
                    color: '#72ACD1'
                }
            },
            splitArea: {
                areaStyle: {
                    color: ['rgba(114, 172, 209, 0.2)',
                        'rgba(114, 172, 209, 0.4)', 'rgba(114, 172, 209, 0.6)',
                        'rgba(114, 172, 209, 0.8)', 'rgba(114, 172, 209, 1)'],
                    shadowColor: 'rgba(0, 0, 0, 0.3)',
                    shadowBlur: 10
                }
            },
            axisLine: {
                lineStyle: {
                    color: 'rgba(255, 255, 255, 0.5)'
                }
            },
            splitLine: {
                lineStyle: {
                    color: 'rgba(255, 255, 255, 0.5)'
                }
            }
        },
        {
            indicator: [
                { text: '语文', max: 150 },
                { text: '数学', max: 150 },
                { text: '英语', max: 150 },
                { text: '物理', max: 120 },
                { text: '化学', max: 108 },
                { text: '生物', max: 72 }
            ],
            center: ['75%', '50%'],
            radius: 120
        }
    ],
    series: [
        {
            name: '雷达图',
            type: 'radar',
            emphasis: {
                lineStyle: {
                    width: 4
                }
            },
            data: [
                {
                    value: [100, 8, 0.40, -80, 2000],
                    name: '图一',
                    symbol: 'rect',
                    symbolSize: 5,
                    lineStyle: {
                        type: 'dashed'
                    }
                },
                {
                    value: [60, 5, 0.30, -100, 1500],
                    name: '图二',
                    areaStyle: {
                        color: 'rgba(255, 255, 255, 0.5)'
                    }
                }
            ]
        },
        {
            name: '成绩单',
            type: 'radar',
            radarIndex: 1,
            data: [
                {
                    value: [120, 118, 130, 100, 99, 70],
                    name: '张三',
                    label: {
                        show: true,
                        formatter: function(params) {
                            return params.value;
                        }
                    }
                },
                {
                    value: [90, 113, 140, 30, 70, 60],
                    name: '李四',
                    areaStyle: {
                        opacity: 0.9,
                        color: new echarts.graphic.RadialGradient(0.5, 0.5, 1, [
                            {
                                color: '#B8D3E4',
                                offset: 0
                            },
                            {
                                color: '#72ACD1',
                                offset: 1
                            }
                        ])
                    }
                }
            ]
        }
    ]
}
				 guanxi.setOption(option3); 
	