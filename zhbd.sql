/*
 Navicat Premium Dump SQL

 Source Server         : 张家口步道
 Source Server Type    : MySQL
 Source Server Version : 80034 (8.0.34)
 Source Host           : rm-8vb9y7j4hm08y9z61xo.mysql.zhangbei.rds.aliyuncs.com:3306
 Source Schema         : zhbd

 Target Server Type    : MySQL
 Target Server Version : 80034 (8.0.34)
 File Encoding         : 65001

 Date: 07/12/2024 17:43:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_article
-- ----------------------------
DROP TABLE IF EXISTS `app_article`;
CREATE TABLE `app_article`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `html` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `video` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_article
-- ----------------------------
INSERT INTO `app_article` VALUES (1, 'xxxx', 1, '内容', '惺惺惜惺惺', NULL, '2023-01-10 12:47:32');
INSERT INTO `app_article` VALUES (2, '滚动2', 1, '内容', '嘻嘻嘻', NULL, '2023-01-12 19:48:38');
INSERT INTO `app_article` VALUES (3, '滚动3', 1, '视频', NULL, 'https://www.runoob.com/try/demo_source/movie.mp4', '2023-01-12 19:48:44');

-- ----------------------------
-- Table structure for app_device
-- ----------------------------
DROP TABLE IF EXISTS `app_device`;
CREATE TABLE `app_device`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备cid',
  `device_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备验证码',
  `brand` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '品牌',
  `model` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '型号',
  `face_lib` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸库id',
  `info_id` int NULL DEFAULT NULL COMMENT '跑道id',
  `dept_id` int NULL DEFAULT NULL COMMENT '项目id',
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '安装位置',
  `status` int NULL DEFAULT NULL COMMENT '离线0 在线1 损坏2',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据权限',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `switch_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '远程开关',
  `switch_date` datetime NULL DEFAULT NULL COMMENT '开关安装日期',
  `switch_open` int NULL DEFAULT 0,
  `switch_status` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_device
-- ----------------------------
INSERT INTO `app_device` VALUES (1, 'bc:9b:5e:70:69:be', 'RSvdkt', '慧人', '12mm机枪', NULL, 4, 1, '大屏厂家测试一号摄像头', 1, 'hr', '2021-10-29 19:33:19', '2023-04-04 20:31:11', NULL, NULL, 0, 0);
INSERT INTO `app_device` VALUES (2, 'bc:9b:5e:70:69:be1', '5dtw89', '慧人', '12mm机枪', NULL, 4, 1, '大屏厂家测试二号摄像头', 1, 'hr', '2021-10-29 19:44:26', '2023-04-04 20:31:13', NULL, NULL, 0, -1);
INSERT INTO `app_device` VALUES (47, '111', NULL, NULL, NULL, NULL, 4, 1, NULL, 0, NULL, '2023-08-26 11:41:15', '2023-08-26 11:41:15', NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for app_device_conf
-- ----------------------------
DROP TABLE IF EXISTS `app_device_conf`;
CREATE TABLE `app_device_conf`  (
  `device_id1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '摄像头A id',
  `device_id2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '摄像头B id',
  `distance` int NULL DEFAULT NULL COMMENT '距离,单位：米',
  PRIMARY KEY (`device_id1`, `device_id2`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '步道配置，配置每两个摄像头之间的距离\r\n每两个摄像头之间的最短距离' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_device_conf
-- ----------------------------
INSERT INTO `app_device_conf` VALUES ('bc:9b:5e:70:69:be', 'bc:9b:5e:70:69:be', 360);

-- ----------------------------
-- Table structure for app_discern
-- ----------------------------
DROP TABLE IF EXISTS `app_discern`;
CREATE TABLE `app_discern`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '摄像id',
  `info_id` int NULL DEFAULT NULL COMMENT '步道id',
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '位置',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `type` int NULL DEFAULT NULL COMMENT '0-开始运动、1-过程识别（距离上一次识别时间超过20min，0）',
  `run_type` int NULL DEFAULT NULL COMMENT '0-走路、1-跑步、2-冲刺',
  `speed` double NULL DEFAULT NULL COMMENT '跑步速度 m/s',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据权限，根据trail_id判断',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `avator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户头像照片路径',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `length` int NULL DEFAULT NULL COMMENT '跑步距离',
  `kcal` double NULL DEFAULT NULL COMMENT '消耗卡路里',
  `run_date` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '运动日期',
  `run_time` int NULL DEFAULT NULL COMMENT '运动时间，秒',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '//摄像头识别记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_discern
-- ----------------------------
INSERT INTO `app_discern` VALUES (1, '1', 4, '大屏厂家测试一号摄像头', 1, 0, 1, 3, 'hr', 'gg', '/upload/hr_bd_shenyang_slgy_admin/20221227/428301598066737152.jpg', '2023-04-04 20:33:19', '2023-04-04 20:34:13', 1000, 5, '20230404', 300);

-- ----------------------------
-- Table structure for app_img
-- ----------------------------
DROP TABLE IF EXISTS `app_img`;
CREATE TABLE `app_img`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_img
-- ----------------------------
INSERT INTO `app_img` VALUES (50, '/root/tomcat/webapps//upload/ROOT/20230110/1.jpg', '2023-01-10 14:59:07');
INSERT INTO `app_img` VALUES (51, '/root/tomcat/webapps//upload/ROOT/20230110/2.jpg', '2023-01-10 14:59:09');
INSERT INTO `app_img` VALUES (52, '/root/tomcat/webapps//upload/ROOT/20230110/3.jpg', '2023-01-10 14:59:10');
INSERT INTO `app_img` VALUES (53, '/root/tomcat/webapps//upload/ROOT/20230110/4.jpg', '2023-01-10 14:59:11');
INSERT INTO `app_img` VALUES (54, '/root/tomcat/webapps//upload/ROOT/20230110/5.jpg', '2023-01-10 14:59:32');
INSERT INTO `app_img` VALUES (55, 'D:/work/zhbd-bj/org.apache.catalina.core.ApplicationPart@7727b8d', '2023-04-04 19:57:30');

-- ----------------------------
-- Table structure for app_info
-- ----------------------------
DROP TABLE IF EXISTS `app_info`;
CREATE TABLE `app_info`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '跑道名称',
  `dept_id` int NULL DEFAULT NULL COMMENT '所属公园id',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '市',
  `county` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '县',
  `length` int NULL DEFAULT 0 COMMENT '长度 米',
  `person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'logo',
  `copyright` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '版权',
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公司',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据权限',
  `start_time` datetime NULL DEFAULT NULL,
  `end_time` datetime NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1 COMMENT '启用禁用',
  `area` json NULL,
  `jfgou_loginname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '加菲狗登录账户（脸库下发使用）',
  `jfgou_password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '加菲狗密码',
  `jfgou_group_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '摄像头管理端分组的id',
  `device_group_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '本地人脸库id',
  `applet_qrcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '小程序二维码图片路径',
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '步道缩略图图片路径',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_info
-- ----------------------------
INSERT INTO `app_info` VALUES (4, '东正华泰智慧步道', 1, '北京市', '市辖区', '朝阳区', 5000, '付伟', '15801601074', '/img/logo.jpg', '©北京东正华泰科技有限公司', '北京慧人智能科技有限公司', 'hr', '2021-06-21 11:58:36', '2021-06-22 11:58:38', 1, '[\"110000\", \"110100\", \"110105\"]', 'yx_fuwei@163.com', 'fuwei1233', '20210621115919mSm1c7TD0ZmEbSIJVr', 'MjAyMTA1MjcwOTQzMDY=hbuQ317CMXknktvu9d', '/img/appqrcode.png', '/img/suoluetu.png');

-- ----------------------------
-- Table structure for app_person
-- ----------------------------
DROP TABLE IF EXISTS `app_person`;
CREATE TABLE `app_person`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `person_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `jfgou_loginname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `jfgou_password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `jfgou_group_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `device_group_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `tbl_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类别',
  `tbl_id` int NULL DEFAULT NULL COMMENT '值id',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `avator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '照片',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_person
-- ----------------------------

-- ----------------------------
-- Table structure for app_plan
-- ----------------------------
DROP TABLE IF EXISTS `app_plan`;
CREATE TABLE `app_plan`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `type` int NULL DEFAULT NULL COMMENT '0-里程，1-时长，2-热量',
  `number` double NULL DEFAULT NULL COMMENT '运动目标，里程是米，时长是秒，热量是kcal',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_plan
-- ----------------------------

-- ----------------------------
-- Table structure for app_plan_log
-- ----------------------------
DROP TABLE IF EXISTS `app_plan_log`;
CREATE TABLE `app_plan_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `run_date` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '运动日期',
  `plan_type` int NULL DEFAULT NULL COMMENT '计划类型 0-里程，1-时长，2-热量',
  `plan_number` int NULL DEFAULT NULL COMMENT '计划数值（运动目标，里程是米，时长是秒，热量是kcal）',
  `is_complete` int NULL DEFAULT NULL COMMENT '是否完成，0未完成，1完成',
  `number` int NULL DEFAULT NULL COMMENT '实际完成数值',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据新增时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_plan_log
-- ----------------------------

-- ----------------------------
-- Table structure for app_runinfo
-- ----------------------------
DROP TABLE IF EXISTS `app_runinfo`;
CREATE TABLE `app_runinfo`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `info_id` int NULL DEFAULT NULL COMMENT '步道id',
  `dept_id` int NULL DEFAULT NULL COMMENT 'deptid',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户手机',
  `step` int NULL DEFAULT 0 COMMENT '步数',
  `mileage` int NULL DEFAULT NULL COMMENT '里程：米',
  `run_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日期：2020-03-11精确到日即可',
  `kcal` double(11, 3) NULL DEFAULT NULL COMMENT '消耗热量',
  `run_time` int NULL DEFAULT NULL COMMENT '持续运动时间：秒',
  `speed` decimal(10, 2) NULL DEFAULT NULL COMMENT '平均速度：km/h',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据权限',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `run_type` int NULL DEFAULT NULL COMMENT '0-走路、1-跑步、2-冲刺',
  `avator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '运动记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_runinfo
-- ----------------------------
INSERT INTO `app_runinfo` VALUES (5, 4, 1, 1, 'hh', '4382568907183', 300, 360, '2023-04-06', 26.107, 87000, 4.00, 'hr', '2023-04-06 10:30:52', '2023-04-06 12:09:23', 2, '/upload/hr_bd_shenyang_slgy_admin/20221227/428297800678113280.jpg');

-- ----------------------------
-- Table structure for app_screen
-- ----------------------------
DROP TABLE IF EXISTS `app_screen`;
CREATE TABLE `app_screen`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `imei` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `info_id` int NULL DEFAULT NULL COMMENT '步道id',
  `info_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `dept_id` int NULL DEFAULT NULL COMMENT '部门id',
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '位置',
  `app_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸识别appid',
  `sdk_key` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸识别sdkkey',
  `arc_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸识别验证码',
  `status` int NULL DEFAULT 1 COMMENT '正常异常',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据',
  `switch_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '开关设备号',
  `switch_date` datetime NULL DEFAULT NULL COMMENT '开关添加日期',
  `remote_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '远程连接id',
  `remote_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '远程连接验证码',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '屏幕管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_screen
-- ----------------------------
INSERT INTO `app_screen` VALUES (3, '9538b247ba6001d1', 4, '森林公园智能步道', 1, '公司测试电视', '5gTJ7MDf9zgPHo9cK2bNJLFTg4LCnqMNi3KMc6j8Ktb8', '7NT3nyRWxm6G6DNcuY6mDQcBQkwzERikFZcKgZMbiXH7', '085F-118B-K44G-UBVG', 1, 'hr', '868739056201191', '2021-07-26 00:00:00', '132690935', 'fuwei123', '2021-06-22 13:35:45', '2023-05-23 16:30:59');
INSERT INTO `app_screen` VALUES (5, '7b1f1667d775927b', 4, '森林公园智能步道', 1, 'ycj平板', '5gTJ7MDf9zgPHo9cK2bNJLFTg4LCnqMNi3KMc6j8Ktb8', '7NT3nyRWxm6G6DNcuY6mDQcBQkwzERikFZcKgZMbiXH7', '85F1-118B-F13C-886R', 1, 'hr', NULL, NULL, '2', '3', '2021-06-26 00:07:49', '2023-05-23 16:30:22');
INSERT INTO `app_screen` VALUES (75, '494d1ef4b92301d3', 4, '森林公园智能步道', 1, 'TEST', NULL, NULL, NULL, 1, 'hr', NULL, NULL, NULL, NULL, '2023-05-23 16:30:15', '2023-05-24 15:55:26');

-- ----------------------------
-- Table structure for app_small
-- ----------------------------
DROP TABLE IF EXISTS `app_small`;
CREATE TABLE `app_small`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `imei` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `info_id` int NULL DEFAULT 4,
  `dept_id` int NULL DEFAULT 1,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `textsize` int NULL DEFAULT 24 COMMENT '标题大小',
  `textcolor` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '#A1EDEA' COMMENT '标题颜色',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_small
-- ----------------------------
INSERT INTO `app_small` VALUES (1, '172ea4a07347691f', 4, 1, 'http://47.92.69.188/img/logo.png', '测试步道', 24, '#DB1CD8', '2023-08-24 11:18:54');

-- ----------------------------
-- Table structure for app_sub
-- ----------------------------
DROP TABLE IF EXISTS `app_sub`;
CREATE TABLE `app_sub`  (
  `imei` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `data` json NULL,
  PRIMARY KEY (`imei`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_sub
-- ----------------------------

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `sex` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '男/女',
  `height` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '170' COMMENT '身高',
  `weight` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '65' COMMENT '体重',
  `age` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '年龄',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `avator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸照片路径',
  `person_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `face_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '虹软的人脸特征码',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新数据时间',
  `info_id` int NULL DEFAULT NULL,
  `dept_id` int NULL DEFAULT NULL,
  `openid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `source` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '注册来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES (2, '66', NULL, '4132518919244', '男', '170', '70', '31', 'hr', '/upload/ROOT/2023-05-24/459599642976321536.jpg', NULL, 'AID6RAAAnEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAt92wPdKoZL3mFi89SDgtPXaQ2Lyer4I9Sn36vec6z7zCCTi90ORavK5tAjxZAvk8mPR/\nvTPu/rykLog9O6jOvI4O9zxoic28F8EovdWaurth8qq9eq79PGTUrT0YY2M9bLKaPRaMAb1hXpw9\nBN/AvUcxPLxZNgo9QWwqvSzurDzlKd88OevRvKAxXD2IhN69jKnaPedYSLxobZS9hEqkvZjjxDwr\nMqM8BwZEvdPUaD0L+NM9tlhAPT2B4rz5HPs8l4LOPYcdHr0qUak9TL6APU9hMr1TCy29AqwTvp4l\n+DunDyC9vDUlvfARODx6Nt67QtUoPUBpMD063509EobLuxhyQD2Xum89czUBPvszI77Bnpq9vW25\nvFIpRLy+eAs9PhKRPZYeC77Lc6Y7tMsVvey3Dz2oo5i719EBu1lP2T0EeKc86CIVPjvyEb1Gbqy9\n/dgEvak+iz3e0De89PqVuwwF4j1wkKU8VXiEvUSvqL38H8O8FsEzvdJFXj0yE7S8wS6VvTQxjT38\n2nq9dkdPvUBYgL1N4k49Oq49PW5gHz2WkOw9GiqfuzTSvrvzluE9bdRXPGBKt7zpO889Bb4HvQsn\no7nQEy89vf4XvaO4mb2RlGc8+hfEvYyfdT0jceW8DC/+vCao8zyiaIs8NoGiPclH8LtQ06O8ZwS1\nPdDJpr3ipQM+d6Q6valDKT01q7a9gCUePeQQUz1rMaK8tmcEPFscy7v8mK+9tkTou3xARjuLMNC9\n8iLgPXo6Mz2Dk9o7bRxpvR31XL3HkI49YwzrPFmEI73Ds5s9V1i2PfoOET195fG8avaEPXiOfj1g\nSMu5o0G6u63BsLzooqk9QFxnPfhQ8zvv0h89Ioc2PhVbRz3w/TW97kvSPV0akj3Puxu9ObgWvFAt\nrbwpLDE9ulWePcbfeD2QbJ+8QkElPRPnj70icX29roAWPXLw3Tyh7bE9mNaTvaEzw72aSzS9W9SO\nPTjxvb2wbyM9WtgJPb8i7Dztys49Y5o9PbHHzzqOzcQ9LpcTveOaYz3viuO85l8VvXiwiTuVl3O9\nSDjgvK9q/T0fdLW7mk41PbRrET2YTJw9rjtvPNAoGb3qaK89mN2OvX8cQDvnp8I8DAKavcldizzQ\nRlM8tVCEO0RQtb3Cq/o9z7cGvjFIszwZ+Fy9EMOKPffoQLzqm6Q9FA0LvgzkrzyDPjw9c+ANPrLZ\ntD15qIU81e5gO0YRLzxiF8I9l7KjvdJzLL1k3Go9RLGovTFa4T2r+oE93oGdvbWZCr7zMlO9zlXF\nPOshHD23i6m8zpD1PPcRAT79SNg8BKL0uuwwtDuBOxG9uLojvaZ2yj18sGq8LH+FvDQH2zx91zq7\nrBEQvQ==\n', '2023-05-24 16:11:04', '2023-07-22 02:13:00', NULL, 1, NULL, NULL);

-- ----------------------------
-- Table structure for app_user_copy1
-- ----------------------------
DROP TABLE IF EXISTS `app_user_copy1`;
CREATE TABLE `app_user_copy1`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `sex` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '男/女',
  `height` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '170' COMMENT '身高',
  `weight` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '65' COMMENT '体重',
  `age` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '年龄',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `avator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸照片路径',
  `person_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `face_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '虹软的人脸特征码',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新数据时间',
  `info_id` int NULL DEFAULT NULL,
  `dept_id` int NULL DEFAULT NULL,
  `openid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `source` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '注册来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_user_copy1
-- ----------------------------
INSERT INTO `app_user_copy1` VALUES (1, 'gg', 'gg', '5287911160007', '男', '170', '70', '19', 'hr', '/upload/hr_bd_shenyang_slgy_admin/20221227/428301598066737152.jpg', '74559537551300', 'AID6RAAAnEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAHtD8PEheH71EjQo++qNYPDvMtj04glI8jk/ZvVddNTwIGUq97F7bPMGPfjy7GBg9bBoa\nviz0Gb1NzqU9XiOyPHWHqb26OPu8FukqPMuderxjXEy92k1GPTU+gD01bw0+qX6/u/S3GjyGTTk+\nmyUXvXHTtL3gzbi9lFgIPYCACb5O3s69TNuFu3Sn7T35+GE8fagsPaNQZr2Xl108tLfQu405Jj2G\nI9292QurvdmoQz3TulM96ZIBPhkRZrwwGPy8X6KePSQW/Lx+yvE9ECuBvZBd27yC7t68wQr4vVTI\nvLkd1hA9BHQAvTYnjD08H6+9JLf1vAwWyDs5X9C6bVYOPUAlir2RHrI9HgkvPahAJb2c86G9SylR\nPGWFgj0PliS+k5fJPYuDCb5dAoO9fTKuvHF0iT0KI0+9DeR7PJTftz2rfNw9YmycPR83yzxBDXu9\nsD6VPG96DT4I4Vo8nDAaPVwlMD3Xn209TPliO0UEL71HhGO8txefvRIKQLweG/K9XVcHvZVMCT2N\nMQG+QG1svHlNbzxMUl88D04aPitvAT0cq6k9eC31PepDwL1c6aI8cTqPvdoO6b0s8+I83SYVvB2X\nSLwR+ey6w8ORvPyTFTzhqNQ9b3p2vcd6sjsdn1y8uQSZvGYGE72ZlWm9XFSdvAv3dD2LS1e8anN7\nPT/tu7wzmIs9dVB1vSp9XT0n/z89QM9uPbNxEj0b+gu+62OUPJ1bCD3lNIg8SdUvPB7LN73j9X+9\nIfKqPYJzm7y2XM65CqRUPffnhr10j9s913+HvLTObjyLIYg9X7gLvMjVuzvhqWO7N1aWvHItrj0l\ngUM91kyevHrTpr37RE89pudzubib1b11SdA8sPkdPtFZlz1GG9s7yKuMPAnrB70NSAK9i0AxvTva\nETy4gp48eOF1PVY+pT1EZio8DcxCPBtBAL6f+EQ5QfMhvQ3LBrxHw4Q9RX4Avb0wSL29nam837Kz\nOwrmuTtFXWg9rygMvTTfAz2rNRA+mGGqvJ6LPL1ZruE8rZHSPDR8WjzjcQO9gU1YvbTYh72CGa48\nxptAPU9ndDr5V8C7jKAvPRAldT3UQMY9SD6GvMoOuL3GmuS8MXHrOxMmc7xFOFa9eMOHu1W6kT1N\nD728Di+wvJVHH71JxhE9YPGmvQgAnL2tSEC929xLveDjYb1Zv489tVsAvQPnt7x8NcO8ghHsPdFp\nsjz9mM09BHnlvIcGNburKhE90sx1OyTGmL3nmPU8CwuFvd3kEz2aLZ884GSIvXB6er3iXfi9SNjz\nOnMcBz1Y8oo9DVAivF3cJz6jPjk9Wm4DPJROmzzi/fc8pYq+vf38ej09qZM8BaOevdHbcL2STF48\nAaoAPQ==\n', '2023-01-07 12:24:18', '2023-04-04 20:30:55', 4, 1, NULL, '大屏幕');

-- ----------------------------
-- Table structure for app_weather
-- ----------------------------
DROP TABLE IF EXISTS `app_weather`;
CREATE TABLE `app_weather`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `wendu` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '温度-50-50度',
  `text` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '多云晴',
  `shidu` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '湿度1-100',
  `feng` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '东南风',
  `fenglv` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '1-9级风',
  `zhiliang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '空气质量优良一般污染',
  `icon` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标 如100',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_weather
-- ----------------------------
INSERT INTO `app_weather` VALUES (1, '20', '大雨', '80', '北风', '3', '优', '100');

-- ----------------------------
-- Table structure for sys_action
-- ----------------------------
DROP TABLE IF EXISTS `sys_action`;
CREATE TABLE `sys_action`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `load` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'js',
  `clazz` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'java时的类',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `opby` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_action
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `key` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `value` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `add_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '后台框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-green|theme-light', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (100, '后台系统标题', 'admin.title', '慧人智能后台管理系统', 'Y', 'admin', '2019-03-11 15:00:31', 'admin', '2019-03-11 15:00:36', '后台系统的页面标题');
INSERT INTO `sys_config` VALUES (101, '网址统计', 'tojing', '<script>\r\nvar _hmt = _hmt || [];\r\n(function() {\r\n  var hm = document.createElement(\"script\");\r\n  hm.src = \"https://hm.baidu.com/hm.js?c356a9d8c8d217cae1ccd9ad30a8e3a9\";\r\n  var s = document.getElementsByTagName(\"script\")[0]; \r\n  s.parentNode.insertBefore(hm, s);\r\n})();\r\n</script>\r\n', 'N', '', NULL, '', NULL, NULL);
INSERT INTO `sys_config` VALUES (102, '光大视频', 'cebbank.course', '1172,1173,1174,1175,1182,1184,1185,1186,1188,1189,1190,1191,1192,1193,1194,1197,1198,1200', 'N', '', NULL, '', NULL, NULL);
INSERT INTO `sys_config` VALUES (103, '网页关键字', 'keywords', '慧人智能', 'N', '', NULL, '', NULL, NULL);
INSERT INTO `sys_config` VALUES (104, '网页描述', 'description', '慧人智能', 'N', '', NULL, '', NULL, NULL);
INSERT INTO `sys_config` VALUES (105, '查询标签', 'tags', '儿童,妇女,青少年,心理,格式塔', 'N', '', NULL, '', NULL, NULL);
INSERT INTO `sys_config` VALUES (106, '版本控制', 'applet.version', '2.0', 'N', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '机构名',
  `applet_title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '小程序标题',
  `applet_banners` json NULL COMMENT '小程序轮播',
  `copyright` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '小程序版权信息',
  `pid` int NULL DEFAULT 0 COMMENT '上级id',
  `code` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '机构代码',
  `img` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '机构图片',
  `province` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '市',
  `county` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '县',
  `person` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `phone` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `status` int NULL DEFAULT 1 COMMENT '机构状态 1启用 0停用',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `app_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户小程序的appid',
  `app_secret` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户小程序的appsecret',
  `tour_about` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '关于我们',
  `tour_wxqrcode` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '二维码',
  `tour_jianjie` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '公园简介',
  `applets` json NULL COMMENT '[{title:小程序标题,img:缩略图,type:1,2,3}]',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 174 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '沈阳森林公园', '沈阳森林公园', '[\"https://www.aihuiren.com/upload/wxapp/banner/1/1.png\"]', '©北京慧人智能科技有限公司', 0, 'hr', '/upload/trail/20210620/227148442542014464.png', '北京', '北京市', '顺义区', '付伟', '15801601074', 'LH', 0, 1, '2021-05-26 15:11:08', 'wx278390814ba63734', '8240a778d1af7fc46574936e0e46e724', NULL, NULL, NULL, '[{\"id\": \"4\", \"img\": \"/upload/ROOT/20210729/241324893528915968.png\", \"type\": \"智慧步道\", \"title\": \"智慧步道\"}, {\"id\": \"2\", \"type\": \"智能导览\", \"title\": \"智慧导览\"}, {\"id\": \"3\", \"type\": \"陪跑竞速\", \"title\": \"陪跑竞速\"}]');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `key` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `value` json NULL,
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'course' COMMENT '字典类型',
  `opby` int NULL DEFAULT 0,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '产品表', 'infos', '[{\"key\": \"app_info\", \"value\": \"智慧步道\"}, {\"key\": \"tour_screen\", \"value\": \"智能导览\"}, {\"key\": \"race_info\", \"value\": \"陪跑竞速\"}]', 'course', 0, '2018-08-07 12:43:42', '2021-10-27 19:21:46');
INSERT INTO `sys_dict` VALUES (2, '项目类别', 'prjs', '[\"智慧步道\", \"智能导览\", \"陪跑竞速\", \"智能竞速\"]', 'course', 0, '2018-08-07 12:48:30', '2021-08-11 15:14:33');
INSERT INTO `sys_dict` VALUES (3, '小程序页', 'pages', '[{\"key\": \"a_0\", \"value\": \"首页\"}, {\"key\": \"a_1_1\", \"value\": \"排行榜\"}, {\"key\": \"b_1_2\", \"value\": \"AR太极\"}, {\"key\": \"b_0\", \"value\": \"导览首页\"}, {\"key\": \"c_0\", \"value\": \"陪跑竞速首页\"}]', 'course', 0, '2018-08-07 17:49:50', '2021-10-19 15:30:34');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '任务名',
  `cron` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '定时规则',
  `code` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL COMMENT '执行代码',
  `data` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '执行参数',
  `status` int NULL DEFAULT NULL COMMENT '是否禁用',
  `opby` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '操作人',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '同步人脸下发服务器的token', '0 53 16 * * ?', 'function main(){\r\n	let us=dao.query(\"select id,avator,source from app_user\",Cnd.where(\"source\",\"=\",\"摄像头\"))\r\n	let ids=new ArrayList();\r\n	us.forEach(function(u){\r\n		Fs.deleteFile(conf.getString(\"web.upload\",Jse.webapps)+u.avator)//删除用户照片\r\n		ids.add(u.id)\r\n	})\r\n	dao.delete(\"app_user\",Cnd.where(\"id\",\"in\",ids))\r\n	dao.delete(\"app_discern\",Cnd.where(\"user_id\",\"in\",ids))\r\n	dao.delete(\"app_runinfo\",Cnd.where(\"user_id\",\"in\",ids))\r\n}', '{}', 0, '0', '每天0点删除', '2021-06-18 14:51:45', '2023-01-10 15:26:58');
INSERT INTO `sys_job` VALUES ('2', '定时删除人脸', '0 0 1 * * ?', 'function main(tbl){\r\nlet list=dao.query(\"app_img\")\r\nlet list1=new ArrayList();\r\nif(list.size()>5){\r\n	for(let i=0;i<list.size()-5;i++){\r\n		Fs.deleteFile(list.get(i).img)\r\n		list1.add(list.get(i).id)\r\n	}\r\n}\r\nfor(let i=0;i<list1.size();i++){\r\n	dao.delete(\"app_img\",Cnd.where(\"id\",\"=\",list1.get(i)))\r\n}\r\n}', '{}', 0, '0', '每天1点删除', '2023-01-10 15:26:04', '2023-03-03 15:39:52');
INSERT INTO `sys_job` VALUES ('3', '定时删除数据', '0 9 15 1 * ?', 'function main(){\r\n	dao.update(\"TRUNCATE TABLE app_discern\")\r\n	dao.update(\"TRUNCATE TABLE app_runinfo\")\r\n}', '{}', 0, '0', 'xx', '2023-02-01 15:03:11', '2023-03-03 15:39:54');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建昵称',
  `type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '日志类型',
  `ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '来源IP',
  `tbl` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL COMMENT '请求结果',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `admin_id` int NULL DEFAULT 0,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76457 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (76074, '宋冬雪', '登录成功', '119.57.164.217', '{\"phone\":\"13161479398\",\"captcha\":\"7975\",\"password\":\"123456\"}', 'user/login', 102041, '2020-04-13 17:32:32', '2020-04-13 17:32:32');
INSERT INTO `sys_log` VALUES (76075, '周银平', '登录成功', '119.57.164.217', '{\"phone\":\"18613885462\",\"captcha\":\"5793\",\"password\":\"123456\"}', 'user/login', 103362, '2020-04-13 17:50:53', '2020-04-13 17:50:53');
INSERT INTO `sys_log` VALUES (76076, '张若圣', '登录成功', '119.57.164.217', '{\"phone\":\"dzh\",\"captcha\":\"8292\",\"password\":\"123\"}', 'user/login', 1, '2020-04-13 17:55:08', '2020-04-13 17:55:08');
INSERT INTO `sys_log` VALUES (76077, '张若圣', '登录成功', '119.57.164.217', '{\"phone\":\"dzh\",\"captcha\":\"8431\",\"password\":\"123\"}', 'user/login', 1, '2020-04-13 17:57:47', '2020-04-13 17:57:47');
INSERT INTO `sys_log` VALUES (76078, '吴航', '登录成功', '119.57.164.217', '{\"phone\":\"13891877751\",\"captcha\":\"5243\",\"password\":\"123456\"}', 'user/login', 72520, '2020-04-13 18:00:18', '2020-04-13 18:00:18');
INSERT INTO `sys_log` VALUES (76079, '张若圣', '登录成功', '119.57.164.217', '{\"phone\":\"dzh\",\"captcha\":\"6266\",\"password\":\"123\"}', 'user/login', 1, '2020-04-14 09:51:50', '2020-04-14 09:51:50');
INSERT INTO `sys_log` VALUES (76080, '陈', '登录成功', '119.57.164.217', '{\"phone\":\"15600035683\",\"captcha\":\"6105\",\"password\":\"123456\"}', 'user/login', 23842, '2020-04-14 09:52:48', '2020-04-14 09:52:48');
INSERT INTO `sys_log` VALUES (76081, '周银平', '登录成功', '119.57.164.217', '{\"phone\":\"18613885462\",\"captcha\":\"6596\",\"password\":\"123456\"}', 'user/login', 103362, '2020-04-14 10:04:36', '2020-04-14 10:04:36');
INSERT INTO `sys_log` VALUES (76082, '周银平', '登录成功', '119.57.164.217', '{\"phone\":\"18613885462\",\"captcha\":\"5220\",\"password\":\"123456\"}', 'user/login', 103362, '2020-04-14 10:40:41', '2020-04-14 10:40:41');
INSERT INTO `sys_log` VALUES (76083, '陈', '登录成功', '119.57.164.217', '{\"phone\":\"15600035683\",\"captcha\":\"7454\",\"password\":\"123456\"}', 'user/login', 23842, '2020-04-14 10:58:56', '2020-04-14 10:58:56');
INSERT INTO `sys_log` VALUES (76084, '周银平', '登录成功', '119.57.164.217', '{\"phone\":\"18613885462\",\"captcha\":\"1142\",\"password\":\"123456\"}', 'user/login', 103362, '2020-04-14 12:24:26', '2020-04-14 12:24:26');
INSERT INTO `sys_log` VALUES (76085, '周银平', '登录成功', '119.57.164.217', '{\"phone\":\"18613885462\",\"captcha\":\"5760\",\"password\":\"123456\"}', 'user/login', 103362, '2020-04-14 13:12:35', '2020-04-14 13:12:35');
INSERT INTO `sys_log` VALUES (76086, '周银平', '登录成功', '119.57.164.217', '{\"phone\":\"18613885462\",\"captcha\":\"2477\",\"password\":\"123456\"}', 'user/login', 103362, '2020-04-14 14:06:55', '2020-04-14 14:06:55');
INSERT INTO `sys_log` VALUES (76087, '孙', '登录成功', '119.57.164.217', '{\"phone\":\"18510175631\",\"captcha\":\"2492\",\"password\":\"123456\"}', 'user/login', 104101, '2020-04-14 14:18:27', '2020-04-14 14:18:27');
INSERT INTO `sys_log` VALUES (76088, '孙', '登录成功', '119.57.164.217', '{\"remember\":\"on\",\"password\":\"123456\",\"phone\":\"18510175631\",\"captcha\":\"2819\"}', 'user/login', 104101, '2020-04-14 14:25:58', '2020-04-14 14:25:58');
INSERT INTO `sys_log` VALUES (76089, '陈', '登录成功', '119.57.164.217', '{\"phone\":\"15600035683\",\"captcha\":\"0349\",\"password\":\"123456\"}', 'user/login', 23842, '2020-04-14 14:34:29', '2020-04-14 14:34:29');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '名称',
  `type` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '类型',
  `pid` int NULL DEFAULT 0 COMMENT '上级ID',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '链接',
  `perms` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '权限',
  `icon` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '图标',
  `status` bigint NULL DEFAULT 0 COMMENT '状态',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `target` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '打开方式\r\nmenuItem\r\nmenuBlank',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', '目录', 0, '', '', 'fa fa-asterisk', 1, 100, '', '2018-04-03 12:16:56', '2020-08-19 15:40:04');
INSERT INTO `sys_menu` VALUES (3, '跑道管理', '目录', 0, '', '', 'fa fa-heart', 1, 99, '', '2018-04-03 12:19:19', '2021-05-18 20:41:18');
INSERT INTO `sys_menu` VALUES (6, '菜单管理', '菜单', 1, 'sys/menu/', 'sys:menu', '', 1, 0, '', '2018-04-03 12:20:36', '2021-05-18 22:24:06');
INSERT INTO `sys_menu` VALUES (7, '角色管理', '菜单', 1, 'sys/role/', 'sys:role', '', 1, 0, 'menuItem', '2018-07-29 19:40:19', '2020-05-20 15:24:47');
INSERT INTO `sys_menu` VALUES (8, '任务管理', '菜单', 1, 'sys/job/', 'sys:job', '', 1, 0, 'menuItem', '2018-07-29 19:44:37', '2020-05-20 15:24:47');
INSERT INTO `sys_menu` VALUES (9, '日志管理', '菜单', 1, 'sys/log/', 'sys:log', '', 1, 0, 'menuItem', '2018-07-29 19:49:03', '2020-05-20 15:24:47');
INSERT INTO `sys_menu` VALUES (72, 'AI运动数据采集站', '菜单', 3, 'app/device/index', 'app:device:index', 'fa fa-bars', 1, 3, 'menuItem', '2021-05-18 20:42:52', '2021-07-05 16:15:41');
INSERT INTO `sys_menu` VALUES (74, '跑道列表', '菜单', 3, 'app/info/', NULL, NULL, 1, 4, 'menuItem', '2021-05-18 20:55:36', '2021-05-26 15:15:30');
INSERT INTO `sys_menu` VALUES (75, '用户列表', '菜单', 3, 'app/user/', 'app:user:', '', 1, 1, 'menuItem', '2021-05-18 22:03:01', '2021-05-18 22:23:18');
INSERT INTO `sys_menu` VALUES (77, '运动数据', '菜单', 3, 'app/runinfo/', '', '', 1, 1, 'menuItem', '2021-05-18 22:21:10', '2021-07-12 10:07:47');
INSERT INTO `sys_menu` VALUES (78, '摄像头识别记录', '菜单', 3, 'app/discenr/', 'app:discenr:', NULL, 1, 2, 'menuItem', '2021-05-18 22:39:54', '2021-07-12 10:07:49');
INSERT INTO `sys_menu` VALUES (79, '用户管理', '菜单', 1, 'sys/user/', NULL, NULL, 1, 1, 'menuItem', '2021-05-26 15:06:38', '2021-05-26 15:06:38');
INSERT INTO `sys_menu` VALUES (80, '项目管理', '菜单', 1, 'sys/dept/', NULL, NULL, 1, 2, 'menuItem', '2021-05-26 15:07:31', '2021-06-29 18:42:13');
INSERT INTO `sys_menu` VALUES (81, '排行榜注册大屏幕', '菜单', 3, 'app/screen/', NULL, NULL, 1, 2, 'menuItem', '2021-05-26 17:01:43', '2021-07-05 16:15:56');
INSERT INTO `sys_menu` VALUES (103, '字典管理', '菜单', 1, 'sys/dict/index', NULL, NULL, 1, 1, 'menuItem', '2021-08-11 15:04:32', '2021-08-11 15:04:32');
INSERT INTO `sys_menu` VALUES (108, '信息发布', '菜单', 1, 'app/article/', '', '', 1, 0, 'menuItem', '2023-01-10 12:00:16', '2023-01-10 12:00:31');
INSERT INTO `sys_menu` VALUES (109, '小屏管理', '菜单', 3, 'app/small/', '', '', 1, 0, 'menuItem', '2023-08-24 11:47:13', '2023-08-24 11:47:43');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `unit_id` int NULL DEFAULT 0,
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `opby` int NULL DEFAULT 0,
  `sort` int NULL DEFAULT 0,
  `status` int NULL DEFAULT 1,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '系统总账户', 0, 0, 1, '2018-01-27 05:46:10', '2020-04-01 15:38:11');
INSERT INTO `sys_role` VALUES (2, '管理员', 'jw', 1, '管理员', 1, 1, 1, '2018-01-27 05:46:42', '2022-08-24 12:49:16');
INSERT INTO `sys_role` VALUES (6, '开发人员', 'dev', 0, '开发', 0, 1, 0, '2021-09-24 22:00:45', '2022-08-24 12:49:11');
INSERT INTO `sys_role` VALUES (7, '步道客户演示', 'trace_user_demo', 0, NULL, 0, 1, 1, '2021-11-22 15:55:16', '2022-08-24 12:47:04');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int NOT NULL,
  `menu_id` int NOT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 72);
INSERT INTO `sys_role_menu` VALUES (2, 74);
INSERT INTO `sys_role_menu` VALUES (2, 75);
INSERT INTO `sys_role_menu` VALUES (2, 77);
INSERT INTO `sys_role_menu` VALUES (2, 78);
INSERT INTO `sys_role_menu` VALUES (2, 81);

-- ----------------------------
-- Table structure for sys_router
-- ----------------------------
DROP TABLE IF EXISTS `sys_router`;
CREATE TABLE `sys_router`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '匹配url',
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '>>转发->重定向',
  `tourl` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '转的url',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `opby` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_router
-- ----------------------------

-- ----------------------------
-- Table structure for sys_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task`  (
  `id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '任务名',
  `jobclass` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '执行类',
  `note` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '任务说明',
  `cron` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '定时规则',
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '执行代码',
  `data` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL COMMENT '执行参数',
  `disabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用',
  `opby` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_task
-- ----------------------------
INSERT INTO `sys_task` VALUES ('66756e943da54cafaef2e8d372c62003', '数据库备份任务', 'jse.quartz.JseJob', '每天凌晨4点执行备份任务', '0 0 4 * * ?', 'MysqlBak.exec(tbl.database, tbl.path);\r\nprint(\"执行备份完成\")', '{\"database\": \"iepsy\",\"path\": \"c:/\"}', 0, '1', '2018-06-19 11:57:56', '2018-10-29 16:18:24');
INSERT INTO `sys_task` VALUES ('e414af41bbd44d22b583ec153166288f', '测试任务', 'jse.quartz.JseJob', '每五秒内打印一段文字', '*/5 * * * * ?', 'print(\"测试任务啊付伟啊\")', '{\"hi\": \"Wechat:wizzer | send red packets of support,thank u\"}', 0, '0', '2018-06-19 11:36:33', '2018-10-29 16:36:41');
INSERT INTO `sys_task` VALUES ('e6c371aa655148ce944a61f5e728dd8d', '测试任务2', 'jse.quartz.JseJob', '每月最后一天23点执行测试', '0 0 23 L * ?', 'print(tbl.qq)', '{qq:57094201}', 0, '1', '2018-06-19 12:08:42', '2018-10-29 16:08:14');

-- ----------------------------
-- Table structure for sys_url
-- ----------------------------
DROP TABLE IF EXISTS `sys_url`;
CREATE TABLE `sys_url`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `opby` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_url
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sex` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '男',
  `role_id` int NULL DEFAULT 0,
  `dept_id` int NULL DEFAULT 0,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `code` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '数据权限',
  `status` int NULL DEFAULT 0,
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '慧人智能',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '超级管理', 'admin', '123', '15801601074', '男', 1, 0, '2020-04-28 17:34:11', 'hr', 1, NULL, '东正华泰');
INSERT INTO `sys_user` VALUES (2, '付伟', 'fw', '123', '15801601074', '男', 2, 1, '2020-05-07 17:52:19', 'hr', 1, NULL, '东正华泰');
INSERT INTO `sys_user` VALUES (16, 'dzh', 'dzh', '123', '15011111116', '男', 1, 0, '2021-06-16 14:40:39', 'hr', 1, '1122', '东正华泰');

SET FOREIGN_KEY_CHECKS = 1;
